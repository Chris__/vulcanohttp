# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- A way to configure the address-port combinations the server listens on
- Support for IPv6
- Support for huffman encoding providing header compression
### Changed
- HTTPResponse constructor to be more generic, so that it can deal with more types of body data
- Use spdlog as new logging system

## [0.0.1] - 2022-01-14
### Added

- Event-based TCP server
- HTTP/2 server which builds on top of the event-based TCP server
- Full implementation of HTTP/2 connection preface
- User-addable request handlers
- TLS support
- Use of ALPN to communicate HTTP/2 support in TLS connections
- PING frame response
- Exceptions
- Documentation generation
- Unit testing
- HTTP Responses (creating and sending)

[Unreleased]: https://gitlab.com/Chris__/vulcanohttp/-/compare/v0.0.1...master?from_project_id=29299648
[0.0.1]: https://gitlab.com/Chris__/vulcanohttp/-/tags/v0.0.1