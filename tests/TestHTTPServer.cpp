/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/HTTPFrameUtils.h"
#include "../src/VulcanoHTTPExceptions.h"
#include <arpa/inet.h>
#include <catch2/catch.hpp>
#include <curl/curl.h>
#include <netdb.h>
#include <vulcanohttp/HTTPResponse.h>
#include <vulcanohttp/HTTPServer.h>
#include <fstream>

static size_t writeCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

TEST_CASE("Handling of normal Request")
{
    using namespace std::string_literals;

    HTTPServer testHTTPServer("test-certificate.pem", "test-privatekey.pem", {{INADDR_ANY, 65455}});
    testHTTPServer.addRequestHandler("^/$", [&testHTTPServer](const HTTPRequest& request) {
        HTTPResponse(
            testHTTPServer,
            request,
            {HTTP::FrameUtils::HTTPHeaderField{8},
             {HTTP::FrameUtils::INDEXED_LITERAL, 31, "text/html; charset=utf-8"}},
            R"(<html><body>You are being <a href="https://about.gitlab.com/">redirected</a>.</body></html>)"s).send();
    });

    testHTTPServer.addRequestHandler(".*", [&testHTTPServer](const HTTPRequest& request) {
        REQUIRE(request.getHeaderFieldFromKey(":method").second == "GET");
        HTTPResponse(
            testHTTPServer,
            request,
            {HTTP::FrameUtils::HTTPHeaderField{8},
             {HTTP::FrameUtils::INDEXED_LITERAL, 54, "vulcanohttp"},
             {HTTP::FrameUtils::INDEXED_LITERAL, 31, "text/html; charset=utf-8"}},
            "Hello World"s).send();
    });
    testHTTPServer.start();

    curl_global_init(CURL_GLOBAL_ALL);

    CURL* curl;
    curl = curl_easy_init();

    std::string result;
    std::string url = "https://127.0.0.1:65455/";
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2TLS);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

    using namespace std::literals::chrono_literals;
    std::this_thread::sleep_for(10ms);
    CURLcode res = curl_easy_perform(curl);
    REQUIRE(result == "<html><body>You are being <a href=\"https://about.gitlab.com/\">redirected</a>.</body></html>");

    result = "";
    curl_easy_setopt(curl, CURLOPT_URL, (url + "test").c_str());
    res = curl_easy_perform(curl);
    REQUIRE(result == "Hello World");

    curl_easy_cleanup(curl);
    testHTTPServer.stop();
}
TEST_CASE("Handling of normal Request over IPv6")
{
    using namespace std::string_literals;

    HTTPServer testHTTPServer("test-certificate.pem", "test-privatekey.pem", {{in6addr_any, 61342}});
    testHTTPServer.addRequestHandler("^/$", [&testHTTPServer](const HTTPRequest& request) {
        HTTPResponse(
            testHTTPServer,
            request,
            {HTTP::FrameUtils::HTTPHeaderField{8},
             {HTTP::FrameUtils::INDEXED_LITERAL, 31, "text/html; charset=utf-8"}},
            R"(<html><body>You are being <a href="https://about.gitlab.com/">redirected</a>.</body></html>)"s).send();
    });

    testHTTPServer.addRequestHandler(".*", [&testHTTPServer](const HTTPRequest& request) {
        REQUIRE(request.getHeaderFieldFromKey(":method").second == "GET");
        HTTPResponse(
            testHTTPServer,
            request,
            {HTTP::FrameUtils::HTTPHeaderField{8},
             {HTTP::FrameUtils::INDEXED_LITERAL, 54, "vulcanohttp"},
             {HTTP::FrameUtils::INDEXED_LITERAL, 31, "text/html; charset=utf-8"}},
            "Hello World"s).send();
    });
    testHTTPServer.start();

    curl_global_init(CURL_GLOBAL_ALL);

    CURL* curl;
    curl = curl_easy_init();

    std::string result;
    std::string url = "https://[::1]:61342/";
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2TLS);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

    using namespace std::literals::chrono_literals;
    std::this_thread::sleep_for(10ms);

    CURLcode res = curl_easy_perform(curl);
    REQUIRE(result == "<html><body>You are being <a href=\"https://about.gitlab.com/\">redirected</a>.</body></html>");

    result = "";
    curl_easy_setopt(curl, CURLOPT_URL, (url + "test").c_str());
    res = curl_easy_perform(curl);
    REQUIRE(result == "Hello World");

    curl_easy_cleanup(curl);
    testHTTPServer.stop();
}
TEST_CASE("Handling of invalid requests")
{
    HTTPServer testHTTPServer("test-certificate.pem", "test-privatekey.pem", {{INADDR_ANY, 65451}});
    testHTTPServer.addRequestHandler("^/$", [&testHTTPServer](const HTTPRequest& request) {
        const std::string responseText =
            "<html><body>You are being <a href=\"https://about.gitlab.com/\">redirected</a>.</body></html>";
        std::vector<unsigned char> body(responseText.begin(), responseText.end());
        HTTPResponse response(
            testHTTPServer,
            request,
            {HTTP::FrameUtils::HTTPHeaderField{8},
             {HTTP::FrameUtils::INDEXED_LITERAL, 31, "text/html; charset=utf-8"},
             {HTTP::FrameUtils::INDEXED_LITERAL, 28, std::to_string(body.size())}},
            body);
        response.send();
        // SSL_write(testHTTPServer.getConnections()->operator[](request.clientFileDescriptor).ssl, buf.data(), 167);
    });
    testHTTPServer.addRequestHandler(".*", [&testHTTPServer](const HTTPRequest& request) {
        REQUIRE(request.getHeaderFieldFromKey(":method").second == "GET");
        const std::string responseText = "Hello World";
        std::vector<unsigned char> body(responseText.begin(), responseText.end());
        HTTPResponse response(
            testHTTPServer,
            request,
            {HTTP::FrameUtils::HTTPHeaderField{8},
             {HTTP::FrameUtils::INDEXED_LITERAL, 54, "vulcanohttp"},
             {HTTP::FrameUtils::INDEXED_LITERAL, 31, "text/html; charset=utf-8"},
             {HTTP::FrameUtils::INDEXED_LITERAL, 28, std::to_string(body.size())}},
            body);
        response.send();
    });
    testHTTPServer.start();

    SSL_CTX* ctx = SSL_CTX_new(TLS_client_method());
    SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, nullptr);

    /*
     // Uncomment this to save the TLS keys to file for debugging purposes
     SSL_CTX_set_keylog_callback(ctx, [](const SSL* ssl, const char* line) {
        std::fstream file;
        file.open("/home/christian/sslkeylog.log", std::ios_base::out | std::ios_base::app);
        file << line << std::endl;
        file.close();
    });*/

    SSL* ssl = SSL_new(ctx);
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0)
        throw VulcanoHTTPExceptions::ErrnoException<spdlog::level::critical>("There was an error while opening a socket!");

    sockaddr_in serverHostAddress{};
    bzero((char*)&serverHostAddress, sizeof(serverHostAddress));

    serverHostAddress.sin_family = AF_INET;
    serverHostAddress.sin_addr.s_addr = inet_addr("127.0.0.1");
    serverHostAddress.sin_port = htons(65451);
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    if (connect(fd, reinterpret_cast<struct sockaddr*>(&serverHostAddress), sizeof(serverHostAddress)) != 0)
        throw VulcanoHTTPExceptions::ErrnoException<spdlog::level::critical>("There was an error while opening a socket!");
    SSL_set_fd(ssl, fd);
    SSL_connect(ssl);
    SSL_write(ssl, "PRI * HTsTP/2.0\r\n\r\nSM\r\n\r\n", 24);
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
    std::array<char, 502> buffer{};
    REQUIRE( SSL_read(ssl, buffer.data(), 20) == 0); // Test if connection is closed
    SSL_shutdown(ssl);
    close(fd);
    SSL_free(ssl);
    SSL_CTX_free(ctx);
    testHTTPServer.stop();
}