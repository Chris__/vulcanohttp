/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define CATCH_CONFIG_MAIN

#include "../src/HTTPBinaryRepresentationUtils.h"
#include "../src/VulcanoHTTPExceptions.h"
#include "../src/HTTPRequestUtils.h"
#include <catch2/catch.hpp>
#include <spdlog/spdlog.h>

TEST_CASE("Interpreting Binary Integer Representations")
{
    // Encoded within prefix:
    std::vector<unsigned char> buffer = {8, 1, 11, 223, 255, 11, /*actual representation:*/ 0b11100111};
    unsigned int bytesRead = 6;
    REQUIRE(HTTP::BinaryRepresentationUtils::interpretIntegerRepresentation(buffer, bytesRead, 5) == 7);
    REQUIRE(bytesRead == 7);

    // Encoded after the prefix:
    buffer = {2, 11, 21, 3, /*actual representation:*/ 0b01011111, 0b10011010, 0b00001010};
    bytesRead = 4;
    REQUIRE(HTTP::BinaryRepresentationUtils::interpretIntegerRepresentation(buffer, bytesRead, 5) == 1337);
    REQUIRE(bytesRead == 7);

    // Encoded after the prefix, but integer unrealistically big, should throw MalformedHTTPRequestException:

    buffer = {/*actual representation:*/ 0b01011111, 0b10011010, 0b10001010, 0b00011011};
    bytesRead = 0;

    REQUIRE_THROWS_AS(HTTP::BinaryRepresentationUtils::interpretIntegerRepresentation(buffer, bytesRead, 5),
                      HTTP::RequestUtils::MalformedHTTPRequestException);
}
TEST_CASE("Interpreting Binary String Representations")
{
    std::vector<unsigned char> buffer = {0,   6,   14,  /*actual representation: String Length:*/ 0b00010011,
                                         'H', 'e', 'l', 'l',
                                         'o', '!', ' ', 'G',
                                         'o', 'o', 'd', ' ',
                                         'M', 'o', 'r', 'n',
                                         'i', 'n', 'g'};
    unsigned int bytesRead = 3;
    REQUIRE(HTTP::BinaryRepresentationUtils::interpretStringRepresentation(buffer, bytesRead) == "Hello! Good Morning");
}
TEST_CASE("Constructing Binary Integer Representations")
{
    std::vector<unsigned char> buffer = {};
    std::vector<unsigned char> checkBuffer = {31, 154, 10};
    unsigned int length = HTTP::BinaryRepresentationUtils::constructIntegerRepresentation(5, 0,1337, 0, buffer);
    REQUIRE(length == 3);
    REQUIRE(buffer == checkBuffer);

    buffer = {};
    checkBuffer = {0, 15};
    length = HTTP::BinaryRepresentationUtils::constructIntegerRepresentation(5,0, 15, 1, buffer);
    REQUIRE(length == 1);
    REQUIRE(buffer == checkBuffer);

    REQUIRE_THROWS_AS(HTTP::BinaryRepresentationUtils::constructIntegerRepresentation(34,0, 12, 0, buffer), VulcanoHTTPExceptions::CustomVulcanException<spdlog::level::err>);
    REQUIRE_THROWS_AS(HTTP::BinaryRepresentationUtils::constructIntegerRepresentation(0,0, 33, 1, buffer), VulcanoHTTPExceptions::CustomVulcanException<spdlog::level::err>);
}
TEST_CASE("Constructing Binary String Representations")
{
    std::vector<unsigned char> buffer = {};
    std::vector<unsigned char> checkBuffer = {0b10000010, 0b11111110, 0b00111111};
    unsigned int length = HTTP::BinaryRepresentationUtils::constructStringRepresentation<true>("!", 0, buffer);
    unsigned int bytesRead = 0;

    REQUIRE(length == 3);
    REQUIRE(HTTP::BinaryRepresentationUtils::interpretStringRepresentation(buffer,bytesRead) == "!");
    REQUIRE(bytesRead == 3);
    REQUIRE(buffer == checkBuffer);

    buffer = {};
    bytesRead = 0;
    checkBuffer = {2, 'a','b'};
    length = HTTP::BinaryRepresentationUtils::constructStringRepresentation<false>("ab", 0, buffer);
    REQUIRE(length == 3);
    REQUIRE(HTTP::BinaryRepresentationUtils::interpretStringRepresentation(buffer,bytesRead) == "ab");
    REQUIRE(bytesRead == 3);
    REQUIRE(buffer == checkBuffer);
}