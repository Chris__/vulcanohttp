# Compact free & open-source HTTP/2 server library for C++

## Overview

VulcanoHTTP is a compact free and open-source HTTP/2 server library for C++. It is written in C++ and builds with CMake.
The name stems from the small island north of Sicily.

## Supported Platforms

Currently, it only supports linux.

## Building

The library is built with CMake, so make sure you have version 3.19 or higher installed. You also need OpenSSL. To
build, execute this from the root directory (the one with the "src" folder)

    mkdir build-output
    cmake . -B build-output
    cd build-output
    make

This builds the library as shared library, usually named "libVulcanoHTTP.so" on Linux.

## FAQ and Troubleshooting

If you face any problems, please take a look at
the [FAQ & Troubleshooting-Guide](https://gitlab.com/Chris__/fireamp-server/-/wikis/FAQ/FAQ-&-Troubleshooting#building)

## Binaries

For each commit binaries are built with GitLab CI as artifacts.

## Development Process
The current version isn't yet production ready, but development is going on.
- [ ] HTTP/2 Server Implementation
  - [x] Event-Based TCP Sockets
  - [x] HTTP Request processing
  - [ ] Other Frames
  - [ ] Fix large frame handling
  - [ ] SSE (Server-Sent Events)

## Bugreports and Feature requests

Please submit an issue to this repo if you find any bugs or have an idea for a feature. If the bug is security-relevant,
please mark the issue as confidential. For more information read the [Contribution Guidelines](CONTRIBUTING.md).

## Contributions

Contributions are welcome, and it would be amazing if you want to help. Refer to
the [Contribution Guidelines](CONTRIBUTING.md) for more information.

## Contributors

All the contributors will be listed in the THANKS file.

## License

VulcanoHTTP is licensed under version 3 or later of the LGPL license, which is a free-as-in-freedom weak-copyleft
license. It allows you to do almost anything as long as you, if you use a modified version of the library in your
program and distribute it, provide this modified version again under the LGPLv3 and make it possible for the user of
your software to switch the library out for another modified version (for example, by dynamically linking it). You also
have to include the license and copyright notice. You can read the license [here](LICENSE).
