/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_HTTPRESPONSE_H
#define VULCANOHTTP_HTTPRESPONSE_H

#include "../../src/HTTPFrameUtils.h"
#include "../../src/HTTPRequestResponseCommon.h"
#include "HTTPServer.h"

/**
 * @brief Class providing a HTTP response data structure, used in combination with an HTTPServer
 *
 * @details A way to formulate an HTTP request to be sent in an HTTP/2 connection
 */
template <class T = std::vector<unsigned char>,bool huffmanEncode = true> class HTTPResponse : public HTTPRequestResponseCommon
{
public:
    HTTPServer* context;
    T bodyData;
    void send();
    void addHeaderField(const HTTP::FrameUtils::HTTPHeaderField& headerField);
    HTTPResponse(HTTPServer& context,
                 int clientFileDescriptor,
                 uint32_t streamID,
                 std::initializer_list<HTTP::FrameUtils::HTTPHeaderField> originalHeaderList);
    HTTPResponse(HTTPServer& context,
                 const HTTPRequest& request,
                 std::initializer_list<HTTP::FrameUtils::HTTPHeaderField> originalHeaderList);
    HTTPResponse(HTTPServer& context,
                 int clientFileDescriptor,
                 uint32_t streamID,
                 std::initializer_list<HTTP::FrameUtils::HTTPHeaderField> originalHeaderList,
                 const T& bodyData);
    HTTPResponse(HTTPServer& context,
                 const HTTPRequest& request,
                 std::initializer_list<HTTP::FrameUtils::HTTPHeaderField> originalHeaderList,
                 const T& bodyData);

private:
    std::vector<HTTP::FrameUtils::HTTPHeaderField> headerList;
};

// Include definitions, they have to be in a header file so the compiler can create the definitions for the different
// types which can be used because of the template
#include "HTTPResponse.hxx"

#endif // VULCANOHTTP_HTTPRESPONSE_H
