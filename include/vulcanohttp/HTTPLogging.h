/*
 * Copyright (C) 2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <optional>
#include <utility>
#include "spdlog/logger.h"
#include "spdlog/spdlog.h"

#ifndef VULCANOHTTP_HTTPLOGGING_H
#define VULCANOHTTP_HTTPLOGGING_H

namespace HTTP::Logging {
    extern std::optional<spdlog::logger> logger;
    void setLogger(spdlog::logger newLogger);
    void initializeVulcanoHTTPLogger();
}

// Logging Macros
#define VULCANOHTTP_LOG(loglevel,...) SPDLOG_LOGGER_CALL(HTTP::Logging::logger, spdlog::level::loglevel, __VA_ARGS__)
//#define VULCANOHTTP_ERRNOLOG(loglevel,message,...) SPDLOG_LOGGER_CALL(HTTP::Logging::logger,spdlog::level::loglevel,"message: {errnumber} - {errstring}",fmt::arg("errnumber", errno), fmt::arg("errstring", std::strerror(errno)),__VA_ARGS__)
#define VULCANOHTTP_ERRNOLOG(loglevel,message,...) SPDLOG_LOGGER_CALL(HTTP::Logging::logger,spdlog::level::loglevel,"message: {errnumber} - {errstring}",fmt::arg("errnumber", errno), fmt::arg("errstring", std::strerror(errno)))
#endif // VULCANOHTTP_HTTPLOGGING_H
