/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_HTTPSERVER_H
#define VULCANOHTTP_HTTPSERVER_H

#include "../../src/HTTPRequestUtils.h"
#include "../../src/event-tcp-server/EventBasedTCPSocket.h"
#include "../../src/HTTPFrameUtils.h"
#include <map>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <regex>

/**
 * @brief Class providing a customizable HTTP/2 server
 *
 * @details A HTTP/2 server which handles requests based on user-added request handlers
 */
class HTTPServer : public EventBasedTCPSocket
{
    enum State
    {
        WAITING_FOR_MAGIC,
        EXPECTING_SETTINGS,
        CONNECTION_PREFACE_FINISHED
    };
    enum StreamState
    {
        WAITING_FOR_HEADERS = 0,
        RECEIVING_HEADERS,
        RECEIVING_DATA,
        RECEIVED_HEADERS,
        RECEIVED_HEADERS_AND_DATA
    };
    struct Stream
    {
        StreamState state;
        int compressedHeaderSize;
        std::vector<unsigned char> headers;
        std::vector<unsigned char> data;
    };
    struct Connection
    {
        int highestStreamID = 0;
        State state;
        SSL* ssl;
        std::unordered_map<uint32_t, Stream> streams;
        std::vector<HTTP::RequestUtils::headerField_t>
            ownDynamicTable; // Dynamic Table which is defined by what the server sends to the client. Used to know how
                             // things can be compressed the best
        std::vector<HTTP::RequestUtils::headerField_t> dynamicTable;
    };

public:
    HTTPServer(const char* certFilePath, const char* privateKeyFilePath, std::initializer_list<listeningBindingPair_t> listenOn);
    HTTPServer(SSL_CTX* sslCtx, std::initializer_list<listeningBindingPair_t> listenOn);
    ~HTTPServer();

    using EventBasedTCPSocket::start;
    [[maybe_unused]] void start();
    void addRequestHandler(const std::string& regex, const std::function<void(const HTTPRequest&)>& handler);
    void addRequestHandler(std::regex& regex, const std::function<void(const HTTPRequest&)>& handler);
    [[maybe_unused]] void removeRequestHandlers(const std::string& regex);

    const std::unique_ptr<std::unordered_map<int, Connection>>& getConnections();

private:
    SSL_CTX* ctx;
    const std::vector<listeningBindingPair_t> listenOn;

    std::unique_ptr<std::unordered_map<int, Connection>> connections;
    std::vector<std::pair<std::regex, std::function<void(const HTTPRequest&)>>> requestHandlers;

    std::array<unsigned char, 8192> buf{};

    int length = 0;
    int bytesRead = 0;

    static int alpnCB([[maybe_unused]] [[maybe_unused]] SSL* ssl,
                      const char** out,
                      unsigned char* outlen,
                      const unsigned char* in,
                      uint32_t inlen,
                      [[maybe_unused]] void* arg);

    void setupServer();

    // Overriding EventBasedTCPServer
    int customRead(int fd, std::array<char, 4097>& buffer) override;
    bool onOpen(int fd) override;
    void onMessage(epoll_event epollEvent, std::vector<char>& message) override;
    void onClose(epoll_event epollEvent) override;

    void writeBuffer(int fd);
    void executeHandlerForHTTPRequest(const HTTPRequest& httpRequest);
    // static void closeConnection(SSL* ssl, int fd);
};

#endif // VULCANOHTTP_HTTPSERVER_H
