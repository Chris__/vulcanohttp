/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "../../src/HTTPFrameUtils.h"

template <class T, bool huffmanEncode> void HTTPResponse<T,huffmanEncode>::send()
{
    T buffer;
    buffer.reserve(18 + headerList.size() + bodyData.size());

    if (bodyData.empty()) {

        unsigned int bytesWritten = 9; // 9 Bytes reserved for the frame header, which has to be written later because we
                                       // don't know the payload length yet

        bytesWritten = writeHeaderListToBuffer<huffmanEncode>(bytesWritten, headerList, buffer);
        // Payload Length is the sum of all bytes written minus the frame header
        constructFrameHeader(buffer, bytesWritten - 9, HTTP::FrameUtils::FrameType::HEADERS, 0b00000101, streamID);

        SSL_write(context->getConnections()->operator[](clientFileDescriptor).ssl, buffer.data(), bytesWritten);
    } else {

        unsigned int bytesWritten = 9; // 9 Bytes reserved for the frame header, which has to be written later because we
                                       // don't know the payload length yet

        bytesWritten = writeHeaderListToBuffer<huffmanEncode>(bytesWritten, headerList, buffer);
        // Payload Length is the sum of all bytes written minus the frame header
        constructFrameHeader(buffer, bytesWritten - 9, HTTP::FrameUtils::FrameType::HEADERS, 0b00000100, streamID);
        buffer.resize(buffer.size() + 9 + bodyData.size());
        constructFrameHeader(buffer, bodyData.size(), HTTP::FrameUtils::FrameType::DATA, 0b00000001, streamID, bytesWritten);
        bytesWritten += 9;
        std::copy(bodyData.begin(), bodyData.end(), buffer.begin() + bytesWritten);

        SSL_write(context->getConnections()->operator[](clientFileDescriptor).ssl, buffer.data(), bytesWritten + bodyData.size());
    }
}
template <class T, bool huffmanEncode> void HTTPResponse<T, huffmanEncode>::addHeaderField(const HTTP::FrameUtils::HTTPHeaderField& headerField)
{
    headerList.emplace_back(headerField);
}
template <class T, bool huffmanEncode> HTTPResponse<T, huffmanEncode>::HTTPResponse(HTTPServer& context,
                           int clientFileDescriptor,
                           uint32_t streamID,
                           std::initializer_list<HTTP::FrameUtils::HTTPHeaderField> originalHeaderList,
                           const T& bodyData)
    : context(&context)
    , headerList(originalHeaderList)
    , bodyData(bodyData)
    , HTTPRequestResponseCommon(clientFileDescriptor, streamID)
{
}
template <class T, bool huffmanEncode> HTTPResponse<T, huffmanEncode>::HTTPResponse(HTTPServer& context,
                           const HTTPRequest& request,
                           std::initializer_list<HTTP::FrameUtils::HTTPHeaderField> originalHeaderList)
    : context(&context)
    , headerList(originalHeaderList)
    , HTTPRequestResponseCommon(request.clientFileDescriptor, request.streamID)
{
}
template <class T, bool huffmanEncode> HTTPResponse<T, huffmanEncode>::HTTPResponse(HTTPServer& context,
                           const HTTPRequest& request,
                           std::initializer_list<HTTP::FrameUtils::HTTPHeaderField> originalHeaderList,
                           const T& bodyData)
    : context(&context)
    , headerList(originalHeaderList)
    , bodyData(bodyData)
    , HTTPRequestResponseCommon(request.clientFileDescriptor, request.streamID)
{
}