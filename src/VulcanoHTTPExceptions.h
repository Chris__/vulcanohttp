/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_VULCANOHTTPEXCEPTIONS_H
#define VULCANOHTTP_VULCANOHTTPEXCEPTIONS_H

//#include "vulcanohttp/HTTPLogging.h"
#include "ConsoleLogSink.h"
#include "vulcanohttp/HTTPLogging.h"
#include <cstring>
#include <exception>
#include <openssl/err.h>
#include <spdlog/spdlog.h>
#include <stacktrace>
#include <string>
namespace VulcanoHTTPExceptions
{
    template <spdlog::level::level_enum level> struct ErrnoException : public std::exception
    {
        std::string finalErrorMessage;
        constexpr explicit ErrnoException(const std::string& errorMessage)
            : finalErrorMessage(fmt::format("{} - Error: {}", errorMessage, std::strerror(errno)))
        {
            HTTP::Logging::initializeVulcanoHTTPLogger();
            VULCANOHTTP_LOG(critical, finalErrorMessage);
        }
        [[nodiscard]] const char* what() const noexcept override
        {
            return finalErrorMessage.data();
        }
    };
    template <spdlog::level::level_enum level> struct CustomVulcanException : public std::exception
    {
        std::string finalErrorMessage;
        constexpr explicit CustomVulcanException(const std::string& errorMessage)
            : finalErrorMessage(errorMessage)
        {
            HTTP::Logging::initializeVulcanoHTTPLogger();
            VULCANOHTTP_LOG(critical, finalErrorMessage);
        }
        [[nodiscard]] const char* what() const noexcept override
        {
            return finalErrorMessage.data();
        }
    };
    template <spdlog::level::level_enum level> void logOpenSSLErrorStack() {
        int err;
        while (err = ERR_get_error(), err != 0) {
            HTTP::Logging::logger->log(level, "     {}",ERR_error_string(err, nullptr));
        }
    }
    template <spdlog::level::level_enum level> struct OpenSSLException : public std::exception
    {
        std::string finalErrorMessage;
        constexpr explicit OpenSSLException(const std::string& errorMessage)
            : finalErrorMessage(errorMessage)
        {
            HTTP::Logging::initializeVulcanoHTTPLogger();
            VULCANOHTTP_LOG(critical, finalErrorMessage);
            logOpenSSLErrorStack<spdlog::level::err>();
        }

        [[nodiscard]] const char* what() const noexcept override
        {
            return finalErrorMessage.data();
        }
    };
} // namespace VulcanoHTTPExceptions

#endif // VULCANOHTTP_VULCANOHTTPEXCEPTIONS_H
