/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_HTTPBINARYREPRESENTATIONUTILS_H
#define VULCANOHTTP_HTTPBINARYREPRESENTATIONUTILS_H

#include <string>
#include <vector>
/**
 * @brief Namespace providing functions to construct and interpret binary data representations in the HTTP/2 protocol
 */
namespace HTTP::BinaryRepresentationUtils
{
    unsigned int interpretIntegerRepresentation(std::vector<unsigned char>& buffer,
                                                unsigned int& bytesRead,
                                                unsigned char prefixLength);
    std::string interpretStringRepresentation(std::vector<unsigned char>& buffer, unsigned int& bytesRead);
    template <typename T>
    unsigned int constructIntegerRepresentation(unsigned char prefix,
                                                unsigned char prefixValue,
                                                unsigned int integer,
                                                unsigned int offset,
                                                T& buffer);
    template <bool huffmanEncode, typename T>
    unsigned int
    constructStringRepresentation(const std::string& string, unsigned int offset, T& buffer);
    template <bool huffmanEncode, typename T>
    unsigned int constructStringRepresentation(unsigned char prefixByte,
                                               const std::string& string,
                                               unsigned int offset,
                                               T& buffer);
}; // namespace HTTP::BinaryRepresentationUtils

// Template definitions (have to be in header file)
#include "HTTPBinaryRepresentationUtils.hxx"

#endif // VULCANOHTTP_HTTPBINARYREPRESENTATIONUTILS_H