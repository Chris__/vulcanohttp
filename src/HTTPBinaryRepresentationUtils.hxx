/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HTTPBinaryRepresentationUtils.h"
#include "HTTPHuffmanUtils.h"
#include "HTTPRequestUtils.h"
#include "VulcanoHTTPExceptions.h"
#include <bitset>
#include <iostream>
#include <string>
#include <vector>

template <typename T>
unsigned int HTTP::BinaryRepresentationUtils::constructIntegerRepresentation(unsigned char prefix,
                                                                             unsigned char prefixValue,
                                                                             unsigned int integer,
                                                                             unsigned int offset,
                                                                             T& buffer)
{
    if (prefix < 1 || prefix > 8) {
        throw VulcanoHTTPExceptions::CustomVulcanException<spdlog::level::err>(
            "HTTP::BinaryRepresentationUtils::constructIntegerRepresentation was called with an invalid prefix number. "
            "Please report this bug! - Error: VULCERROR_INVALID-PREFIX-CONSTRUCTING-INT-REP");
    }
    buffer.reserve(buffer.size() + sizeof(int));
    if (buffer.size() < offset + 1) {
        buffer.resize(offset + 1);
    }
    if (integer < (1 << prefix) - 1) {
        buffer[offset] = prefixValue | integer;
        return 1;
    }
    buffer[offset] = buffer[offset] | (1 << prefix) - 1;
    integer -= (1 << prefix) - 1;

    int i = 1;
    for (; integer >= 128; ++i) {
        if (buffer.size() < offset + i + 1) {
            buffer.resize(offset + i + 1);
        }
        buffer[offset + i] = integer & 0b01111111 | 0b10000000;
        integer /= 128;
    }
    if (buffer.size() < offset + i + 1) {
        buffer.resize(offset + i + 1);
    }
    buffer[offset + i] = integer;
    return i + 1;
}
template <bool huffmanEncode, typename T>
unsigned int HTTP::BinaryRepresentationUtils::constructStringRepresentation(const std::string& string,
                                                                                  unsigned int offset,
                                                                                  T& buffer)
{
    if constexpr(huffmanEncode) {
        // Using vector as dynamic bitmap
        std::vector<bool> bitMap;
        bitMap.reserve(string.size() * 8);
        for (unsigned char c : string) {
            bitMap.reserve(bitMap.size() + HTTP::HuffmanUtils::huffmanTable[c].size());
            bitMap.insert(
                bitMap.end(), HTTP::HuffmanUtils::huffmanTable[c].begin(), HTTP::HuffmanUtils::huffmanTable[c].end());
        }
        bitMap.insert(bitMap.end(), 8 - (bitMap.size() % 8), true);
        unsigned int i = constructIntegerRepresentation(7, 0b10000000, bitMap.size() / 8, offset, buffer);

        if (buffer.size() < offset + i + bitMap.size() / 8) {
            buffer.resize(offset + i + bitMap.size() / 8);
        }
        unsigned int j = 0;
        for (; j < bitMap.size() / 8; ++j) {
            for (int currentBit = 0; currentBit < 8; ++currentBit) {
                buffer[offset + i + j] = buffer[offset + i + j] | bitMap[currentBit + j * 8] << (7 - currentBit);
            }
        }
        return i + j;
    } else {
        unsigned int i = constructIntegerRepresentation(7, 0, string.size(), offset, buffer);
        if (buffer.size() < offset + i + string.size()) {
            buffer.resize(offset + i + string.size());
        }
        std::copy(string.begin(), string.end(), buffer.begin() + offset + i);
        return i + string.size();
    }

}
template <bool huffmanEncode,typename T>
unsigned int HTTP::BinaryRepresentationUtils::constructStringRepresentation(unsigned char prefixByte,
                                                                                  const std::string& string,
                                                                                  unsigned int offset,
                                                                                  T& buffer)
{
    if constexpr(huffmanEncode) {
        // Using vector as dynamic bitmap
        std::vector<bool> bitMap;
        bitMap.reserve(string.size() * 8);
        for (unsigned char c : string) {
            bitMap.reserve(bitMap.size() + HTTP::HuffmanUtils::huffmanTable[c].size());
            bitMap.insert(
                bitMap.end(), HTTP::HuffmanUtils::huffmanTable[c].begin(), HTTP::HuffmanUtils::huffmanTable[c].end());
        }
        bitMap.insert(bitMap.end(), 8 - (bitMap.size() % 8), true);
        unsigned int i = constructIntegerRepresentation(7, 0b10000000, bitMap.size() / 8, offset, buffer);

        if (buffer.size() < offset + i + bitMap.size() / 8) {
            buffer.resize(offset + i + bitMap.size() / 8);
        }
        unsigned int j = 0;
        for (; j < bitMap.size() / 8; ++j) {
            for (int currentBit = 0; currentBit < 8; ++currentBit) {
                buffer[offset + i + j] = buffer[offset + i + j] | bitMap[currentBit + j * 8] << (7 - currentBit);
            }
        }

        buffer[offset - 1] = prefixByte;
        return i + j;
    } else {
        unsigned int i = constructIntegerRepresentation(7, 0, string.size(), offset, buffer);
        if (buffer.size() < offset + i + string.size()) {
            buffer.resize(offset + i + string.size());
        }
        std::copy(string.begin(), string.end(), buffer.begin() + offset + i);
        buffer[offset - 1] = prefixByte; // Writing the prefix byte one byte before representation
        return i + string.size();
    }
}

