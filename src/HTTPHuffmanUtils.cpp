/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HTTPHuffmanUtils.h"

#include <memory>
const std::unique_ptr<HTTP::HuffmanUtils::HuffmanTreeNode> httpHuffmanTree =
    []() -> std::unique_ptr<HTTP::HuffmanUtils::HuffmanTreeNode> {
    // Builds the tree from the given huffman code
    int i = 0;
    std::unique_ptr<HTTP::HuffmanUtils::HuffmanTreeNode> rootNode =
        std::make_unique<HTTP::HuffmanUtils::HuffmanTreeNode>();

    for (const HTTP::HuffmanUtils::bits_t& huffmanCodeBits : HTTP::HuffmanUtils::huffmanTable) {
        HTTP::HuffmanUtils::HuffmanTreeNode* current = rootNode.get();
        for (const bool huffmanCodeBit : huffmanCodeBits) {
            if (huffmanCodeBit) {
                if (current->left == nullptr) {
                    current->left = std::make_unique<HTTP::HuffmanUtils::HuffmanTreeNode>();
                }
                current = current->left.get();
            } else {
                if (current->right == nullptr) {
                    current->right = std::make_unique<HTTP::HuffmanUtils::HuffmanTreeNode>();
                }
                current = current->right.get();
            }
        }
        current->leaf = true;
        current->value = static_cast<char>(i);

        i++;
    }
    return rootNode;
}();

const HTTP::HuffmanUtils::HuffmanTreeNode* HTTP::HuffmanUtils::get()
{
    return httpHuffmanTree.get();
}
