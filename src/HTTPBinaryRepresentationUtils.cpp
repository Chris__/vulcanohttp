/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HTTPBinaryRepresentationUtils.h"
#include "HTTPHuffmanUtils.h"
#include "HTTPRequestUtils.h"
#include "VulcanoHTTPExceptions.h"
#include <bitset>
#include <iostream>
#include <string>
#include <vector>

unsigned int HTTP::BinaryRepresentationUtils::interpretIntegerRepresentation(std::vector<unsigned char>& buffer,
                                                                             unsigned int& bytesRead,
                                                                             unsigned char prefixLength)
{
    unsigned char prefix = ((1 << prefixLength) - 1);
    unsigned int index = buffer[bytesRead] & prefix;

    if (index == prefix) {
        unsigned char currentBitCount = 0;
        do {
            if (currentBitCount > 7) { // Limited to 2 runs because more than that isn't realistic
                throw HTTP::RequestUtils::MalformedHTTPRequestException();
            }
            bytesRead += 1;
            index += (buffer[bytesRead] & 127) << currentBitCount;
            currentBitCount += 7;
        } while (buffer[bytesRead] & 128);
    }
    bytesRead += 1;
    return index;
}
std::string HTTP::BinaryRepresentationUtils::interpretStringRepresentation(std::vector<unsigned char>& buffer,
                                                                           unsigned int& bytesRead)
{
    bool isHuffmanCoded = buffer[bytesRead] & 0b10000000;
    unsigned int valueLength = interpretIntegerRepresentation(buffer, bytesRead, 7);
    if (valueLength > 4096) {
        throw HTTP::RequestUtils::MalformedHTTPRequestException();
    }
    if (isHuffmanCoded) {
        std::vector<bool> bits(valueLength * 8);
        for (int currentByte = 0; currentByte < valueLength; ++currentByte) {
            for (int currentBit = 0; currentBit < 8; ++currentBit) {
                bits[currentByte * 8 + currentBit] = (buffer[currentByte + bytesRead] >> (7 - currentBit)) & 1;
            }
        }

        const HTTP::HuffmanUtils::HuffmanTreeNode* currentNode = HTTP::HuffmanUtils::get();
        std::string decodedString;
        decodedString.reserve(valueLength + 20);
        for (bool bit : bits) {
            // TODO: Check for:   Upon decoding, an incomplete code at the end of the encoded data is
            //     to be considered as padding and discarded.  A padding strictly longer
            //     than 7 bits MUST be treated as a decoding error.  A padding not
            //     corresponding to the most significant bits of the code for the EOS
            //     symbol MUST be treated as a decoding error.  A Huffman-encoded string
            //     literal containing the EOS symbol MUST be treated as a decoding
            //     error.
            if (bit) {
                currentNode = currentNode->left.get();
            } else {
                currentNode = currentNode->right.get();
            }
            if (currentNode->leaf) {
                decodedString.append(currentNode->value);
                currentNode = HTTP::HuffmanUtils::get();
            }
        }
        bytesRead += valueLength;
        return decodedString;
    } else {
        std::string result;
        result.resize(valueLength);

        std::copy(buffer.begin() + bytesRead, buffer.begin() + bytesRead + valueLength, result.begin());
        bytesRead += valueLength;
        return result;
    }
}