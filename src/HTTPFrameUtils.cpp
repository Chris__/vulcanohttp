/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HTTPFrameUtils.h"
#include "HTTPBinaryRepresentationUtils.h"
#include <vulcanohttp/HTTPResponse.h>

int HTTP::FrameUtils::constructGoAwayFrame(std::array<unsigned char, 8192>& buffer,
                                           uint32_t lastStream,
                                           ErrorCode errorCode)
{

    constructFrameHeader(buffer, 8, GOAWAY, 0, 0);

    //     +-+-------------------------------------------------------------+
    //    |R|                  Last-Stream-ID (31)                        |
    //    +-+-------------------------------------------------------------+
    //    |                      Error Code (32)                          |
    //    +---------------------------------------------------------------+
    //    |                  Additional Debug Data (*)                    |
    //    +---------------------------------------------------------------+
    //                        GOAWAY Frame Payload

    // Last Stream ID, The last stream identifier can be set to 0 if
    //   no streams were processed.
    buffer[9] = (lastStream >> 24) & 0xFF;
    buffer[10] = (lastStream >> 16) & 0xFF;
    buffer[11] = (lastStream >> 8) & 0xFF;
    buffer[12] = lastStream & 0xFF;

    // Error Code
    buffer[13] = (errorCode >> 24) & 0xFF;
    buffer[14] = (errorCode >> 16) & 0xFF;
    buffer[15] = (errorCode >> 8) & 0xFF;
    buffer[16] = errorCode & 0xFF;

    return 17;
}
int HTTP::FrameUtils::constructSettingsAck(std::array<unsigned char, 8192>& buffer)
{
    constructFrameHeader(buffer, 0, SETTINGS, 1, 0);
    return 9;
}
HTTP::FrameUtils::FrameHeader HTTP::FrameUtils::interpretFrameHeader(const std::array<unsigned char, 8192>& buffer)
{
    FrameHeader interpretedHeader{
        static_cast<uint32_t>((static_cast<unsigned char>(buffer[0]) << 16 | static_cast<unsigned char>(buffer[1]) << 8
                               | static_cast<unsigned char>(buffer[2]))),
        static_cast<FrameType>(buffer[3]),
        buffer[4],
        static_cast<uint32_t>((static_cast<unsigned char>(buffer[5]) << 24 | static_cast<unsigned char>(buffer[6]) << 16
                               | static_cast<unsigned char>(buffer[7]) << 8 | static_cast<unsigned char>(buffer[8])))};
    return interpretedHeader;
}
HTTP::FrameUtils::FrameHeader HTTP::FrameUtils::interpretFrameHeader(const std::vector<char>& buffer,
                                                                     unsigned int offset)
{
    FrameHeader interpretedHeader{
        static_cast<uint32_t>((static_cast<unsigned char>(buffer[0 + offset]) << 16
                               | static_cast<unsigned char>(buffer[1 + offset]) << 8
                               | static_cast<unsigned char>(buffer[2 + offset]))),
        static_cast<FrameType>(buffer[3 + offset]),
        static_cast<unsigned char>(buffer[4 + offset]),
        static_cast<uint32_t>(
            (static_cast<unsigned char>(buffer[5 + offset]) << 24 | static_cast<unsigned char>(buffer[6 + offset]) << 16
             | static_cast<unsigned char>(buffer[7 + offset]) << 8 | static_cast<unsigned char>(buffer[8 + offset])))};
    return interpretedHeader;
}
template <typename T>
int HTTP::FrameUtils::constructDataFrame(uint32_t streamID,
                                         bool endStream,
                                         std::array<unsigned char, 8192>& buffer,
                                         T data)
{
    int dataLength = data.size();
    constructFrameHeader(buffer, dataLength, DATA, endStream, streamID);
    std::copy(data.begin(), data.begin() + dataLength, buffer.begin());

    return 9 + dataLength;
}
int HTTP::FrameUtils::constructAckPingFrame(std::array<unsigned char, 8192>& buffer)
{
    constructFrameHeader(buffer, 8, PING, 1, 0);
    return 17;
}