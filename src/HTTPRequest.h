/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_HTTPREQUEST_H
#define VULCANOHTTP_HTTPREQUEST_H

#include "HTTPRequestResponseCommon.h"
#include <array>
#include <string>
#include <vector>

class HTTPRequest : public HTTPRequestResponseCommon
{
private:
    std::array<bool, 4> necessaryHeaderFields =
        {}; // In order: methodReceived, schemeReceived, pathReceived, authorityReceived
    std::vector<HTTP::RequestUtils::headerField_t>
        headerList; // Shouldn't be replaced with a map cause duplicate fields are sometimes allowed (which are
                    // allowed is user-defined in HTTP Server instance, default is the "cookie" field)
public:
    std::vector<unsigned char> bodyData;

    // TODO: There are sometimes more header fields than one with the same key, which want to be found
    [[nodiscard]] const HTTP::RequestUtils::headerField_t& getHeaderFieldFromKey(std::string_view key) const;
    void addToHeaderList(std::vector<HTTP::RequestUtils::headerField_t>& dynamicTable, unsigned int index);

    void addToHeaderList(std::vector<HTTP::RequestUtils::headerField_t>& dynamicTable,
                         unsigned int index,
                         const std::string& headerFieldValue);
    void addToHeaderListAndDynamicTable(std::vector<HTTP::RequestUtils::headerField_t>& dynamicTable,
                                        unsigned int index,
                                        const std::string& headerFieldValue);
    void addToHeaderList(const std::string& headerFieldKey, const std::string& headerFieldValue);
    void addToHeaderListAndDynamicTable(std::vector<HTTP::RequestUtils::headerField_t>& dynamicTable,
                                        const std::string& headerFieldKey,
                                        const std::string& headerFieldValue);
    virtual void addToHeaderList(const HTTP::RequestUtils::headerField_t& headerField);

    [[nodiscard]] const std::vector<HTTP::RequestUtils::headerField_t>& getDecodedHeaderList() const;

    [[nodiscard]] bool hasAllNecessaryHeaderFields() const;

    HTTPRequest(int clientFileDescriptor, uint32_t streamID);
};

#endif // VULCANOHTTP_HTTPREQUEST_H
