//
// Created by christian on 7/29/22.
//

#ifndef VULCANOHTTP_CONSOLELOGSINK_H
#define VULCANOHTTP_CONSOLELOGSINK_H

#include <spdlog/sinks/base_sink.h>
#include <spdlog/details/log_msg.h>
#include <spdlog/details/null_mutex.h>
#include <spdlog/pattern_formatter.h>

#include <algorithm>
#include <memory>
#include <mutex>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <vector>

/**
 * @brief Class extending spdlog's base_sink to write to stdout and stderr based on logging level
 * @tparam Mutex Template to set the mutex type
 */
template<typename Mutex>
class ConsoleSink : public spdlog::sinks::base_sink<Mutex>
{
public:
    explicit ConsoleSink(spdlog::level::level_enum level = spdlog::level::err ) : level(level)
    {
    }

    ConsoleSink(const ConsoleSink &) = delete;
    ConsoleSink &operator=(const ConsoleSink &) = delete;

    spdlog::sinks::stderr_color_sink_mt a;
    spdlog::sinks::stdout_color_sink_mt b;

    const spdlog::level::level_enum level;
protected:
    void sink_it_(const spdlog::details::log_msg &msg) override
    {
        msg.level >= level ? a.log(msg) : b.log(msg);
    }

    void flush_() override
    {
        a.flush();
        b.flush();
    }

    void set_pattern_(const std::string &pattern) override
    {
        set_formatter_(spdlog::details::make_unique<spdlog::pattern_formatter>(pattern));
    }

    void set_formatter_(std::unique_ptr<spdlog::formatter> sink_formatter) override
    {
        spdlog::sinks::base_sink<Mutex>::formatter_ = std::move(sink_formatter);
        a.set_formatter(spdlog::sinks::base_sink<Mutex>::formatter_->clone());
        b.set_formatter(spdlog::sinks::base_sink<Mutex>::formatter_->clone());
    }
};

using ConsoleLogSink_mt = ConsoleSink<std::mutex>;
using ConsoleLogSink_st = ConsoleSink<spdlog::details::null_mutex>;

#endif // VULCANOHTTP_CONSOLELOGSINK_H
