/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_HTTPFRAMEUTILS_H
#define VULCANOHTTP_HTTPFRAMEUTILS_H

#include "HTTPRequestUtils.h"
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

namespace HTTP::FrameUtils
{
    enum HeaderFieldRepresentationTypes
    {
        INDEXED,
        LITERAL_INDEXED = 1,
        LITERAL_INDEXED_NEW_NAME,
        LITERAL_NOT_INDEXED = 3,
        LITERAL_NOT_INDEXED_NEW_NAME,
        LITERAL_NEVER_INDEXED = 5,
        LITERAL_NEVER_INDEXED_NEW_NAME
    };
    enum LiteralHeaderFieldRepresentationTypes
    {
        INDEXED_LITERAL = 1,
        NOT_INDEXED_LITERAL = 3,
        NEVER_INDEXED_LITERAL = 5
    };
    struct HTTPHeaderField
    {
        HeaderFieldRepresentationTypes type;
        int index = 0;
        HTTP::RequestUtils::headerField_t pair;

        // Constructor for the "Indexed Header Field Representation"
        explicit HTTPHeaderField(int index)
            : type(HeaderFieldRepresentationTypes::INDEXED)
            , index(index)
        {
        }
        HTTPHeaderField(LiteralHeaderFieldRepresentationTypes representationType, int index, const std::string& value)
            : index(index)
            , type(static_cast<HeaderFieldRepresentationTypes>(representationType))
            , pair("", value)
        {
        }
        HTTPHeaderField(LiteralHeaderFieldRepresentationTypes representationType,
                        const std::string& key,
                        const std::string& value)
            : type(static_cast<HeaderFieldRepresentationTypes>(representationType + 1))
            , pair(key, value)
        {
        }
    };
    enum FrameType
    {
        DATA = 0,
        HEADERS,
        PRIORITY,
        RST_STREAM,
        SETTINGS,
        PUSH_PROMISE,
        PING,
        GOAWAY,
        WINDOW_UPDATE,
        CONTINUATION
    };
    enum ErrorCode
    {
        NO_ERROR = 0,
        PROTOCOL_ERROR,
        INTERNAL_ERROR,
        FLOW_CONTROL_ERROR,
        SETTINGS_TIMEOUT,
        STREAM_CLOSED,
        FRAME_SIZE_ERROR,
        REFUSED_STREAM,
        CANCEL,
        COMPRESSION_ERROR,
        CONNECT_ERROR,
        ENHANCE_YOUR_CALM,
        INADEQUATE_SECURITY,
        HTTP_1_1_REQUIRED
    };
    struct FrameHeader
    {
        uint32_t payloadLength;
        FrameType type;
        unsigned char flags;
        uint32_t streamID;
    };

    FrameHeader interpretFrameHeader(const std::array<unsigned char, 8192>& buffer);
    FrameHeader interpretFrameHeader(const std::vector<char>& buffer, unsigned int offset);

    template <typename T>
    void
    constructFrameHeader(T& buffer, uint32_t payloadLength, FrameType type, unsigned char flags, uint32_t streamID);
    template <typename T>
    void constructFrameHeader(T& buffer,
                              uint32_t payloadLength,
                              FrameType type,
                              unsigned char flags,
                              uint32_t streamID,
                              unsigned int offset);
    int constructGoAwayFrame(std::array<unsigned char, 8192>& buffer, uint32_t lastStream, ErrorCode errorCode);
    int constructSettingsAck(std::array<unsigned char, 8192>& buffer);
    template <typename T>
    int constructDataFrame(uint32_t streamID, bool endStream, std::array<unsigned char, 8192>& buffer, T data);
    int constructAckPingFrame(std::array<unsigned char, 8192>& buffer);

    template <bool huffmanEncode, typename T>
    unsigned int writeHeaderListToBuffer(unsigned int bytesWritten,
                                         const std::vector<HTTPHeaderField>& headerList,
                                         T& buffer);

}; // namespace HTTP::FrameUtils

#include "HTTPFrameUtils.hxx"

#endif // VULCANOHTTP_HTTPFRAMEUTILS_H