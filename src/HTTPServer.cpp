/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/vulcanohttp/HTTPServer.h"
#include "../include/vulcanohttp/HTTPLogging.h"
#include "ConsoleLogSink.h"
#include "HTTPFrameUtils.h"
#include "VulcanoHTTPExceptions.h"
#include "vulcanohttp/HTTPResponse.h"
#include <regex>
#include <spdlog/sinks/dist_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <string>
#include <unistd.h>

int HTTPServer::alpnCB([[maybe_unused]] SSL* ssl,
                       const char** out,
                       unsigned char* outLen,
                       const unsigned char* in,
                       uint32_t inLen,
                       [[maybe_unused]] void* arg)
{
    unsigned int srvLen;
    unsigned char* srv;

    srv = (unsigned char*)"\x02h2\x08http/1.1"; // Served Protocols
    srvLen = 12;

    if (SSL_select_next_proto((unsigned char**)out, outLen, srv, srvLen, in, inLen) != OPENSSL_NPN_NEGOTIATED) {
        return SSL_TLSEXT_ERR_NOACK;
    }
    return SSL_TLSEXT_ERR_OK;
}

HTTPServer::HTTPServer(const char* certFilePath, const char* privateKeyFilePath, std::initializer_list<listeningBindingPair_t> listenOn)
    : listenOn(listenOn)
{
    ctx = SSL_CTX_new(TLS_server_method());

    HTTP::Logging::initializeVulcanoHTTPLogger();
    if (SSL_CTX_use_certificate_file(ctx, certFilePath, SSL_FILETYPE_PEM) != 1
        || SSL_CTX_use_PrivateKey_file(ctx, privateKeyFilePath, SSL_FILETYPE_PEM) != 1) {
        throw VulcanoHTTPExceptions::OpenSSLException<spdlog::level::critical>( "Coudn't load certificate and/or private key file. OpenSSL Error Stack is printed below:");
    }

    setupServer();
}

HTTPServer::HTTPServer(SSL_CTX* sslCtx, std::initializer_list<listeningBindingPair_t> listenOn)
    : ctx(sslCtx)
    , listenOn(listenOn)
{
    setupServer();
}
void closeConnection(SSL* ssl, int fd)
{
    SSL_shutdown(ssl);
    close(fd);
    SSL_free(ssl);
}
HTTPServer::~HTTPServer()
{
    for (const std::pair<const int, Connection>& con : *connections) {
        length = HTTP::FrameUtils::constructGoAwayFrame(buf, con.second.highestStreamID, HTTP::FrameUtils::NO_ERROR);
        writeBuffer(con.first);
        closeConnection(con.second.ssl, con.first);
    }
    SSL_CTX_free(ctx);
}
void HTTPServer::setupServer()
{
    SSL_CTX_set_alpn_select_cb(ctx, reinterpret_cast<SSL_CTX_alpn_select_cb_func>(HTTPServer::alpnCB), nullptr);
    std::unique_ptr<std::unordered_map<int, Connection>> connectionList(new std::unordered_map<int, Connection>);
    connections = std::move(connectionList);
}
int HTTPServer::customRead(int fd, std::array<char, 4097>& buffer)
{
    SSL* ssl = connections->operator[](fd).ssl;

    return SSL_read(ssl, buffer.data(), 4096);
}
bool HTTPServer::onOpen(int fd)
{
    // Create SSL context for new connection
    SSL* ssl = SSL_new(ctx);
    SSL_set_fd(ssl, fd);
    int successCode = SSL_accept(ssl);
    if (successCode != 1) {
        SSL_load_error_strings();
        VULCANOHTTP_LOG(warn, "Error on TLS level while establishing connection with new client! - OpenSSL Error Stack follows: ");
        VulcanoHTTPExceptions::logOpenSSLErrorStack<spdlog::level::warn>();

        close(fd);
        SSL_free(ssl);
        return false;
    }

    Connection con {.state = WAITING_FOR_MAGIC,
                    .ssl = ssl};
    connections->operator[](fd) = con;
    return true;
}
void HTTPServer::executeHandlerForHTTPRequest(const HTTPRequest& httpRequest)
{
    bool foundHandler = false;
    for (const auto& handlerPair : requestHandlers) {
        if (std::regex_search(httpRequest.getHeaderFieldFromKey(":path").second, handlerPair.first)) {
            handlerPair.second(httpRequest);
            foundHandler = true;
            break;
        }
    }
    if (!foundHandler) {
        // Send 404
        const std::string notFoundWebSite = "<html><head></head><body><h1 style=\"text-align:center\">404 Not "
                                            "Found</h1></body></html>";
        std::vector<unsigned char> body(notFoundWebSite.begin(), notFoundWebSite.end());
        HTTPResponse(
            *this,
            httpRequest,
            {HTTP::FrameUtils::HTTPHeaderField(13),
             HTTP::FrameUtils::HTTPHeaderField(HTTP::FrameUtils::INDEXED_LITERAL, 31, "text/html; charset=utf-8"),
             HTTP::FrameUtils::HTTPHeaderField(
                 HTTP::FrameUtils::INDEXED_LITERAL, 28, std::to_string(notFoundWebSite.size()))},
            body)
            .send();
    }
}

void HTTPServer::onMessage(epoll_event epollEvent, std::vector<char>& message)
{
    bytesRead = 0;
    Connection* currentConnection = &(connections->operator[](epollEvent.data.fd));

    unsigned long messageLength = message.size();

    while (bytesRead < messageLength) {
        // TODO: May make this cleaner and not all in one
        // TODO: Make sure clients can't do dangerous attacks
        switch (currentConnection->state) {
        case CONNECTION_PREFACE_FINISHED: {
            HTTP::FrameUtils::FrameHeader frameHeader = HTTP::FrameUtils::interpretFrameHeader(message, bytesRead);
            bytesRead += 9;
            switch (frameHeader.type) {
            case HTTP::FrameUtils::DATA: {

                Stream* currentStream = &(currentConnection->streams[frameHeader.streamID]);
                if (frameHeader.streamID == 0 || currentStream->state != RECEIVING_DATA) {
                    length = HTTP::FrameUtils::constructGoAwayFrame(
                        buf, currentConnection->highestStreamID, HTTP::FrameUtils::PROTOCOL_ERROR);
                    writeBuffer(epollEvent.data.fd);
                    closeConnection(currentConnection->ssl, epollEvent.data.fd);
                    return;
                }

                currentStream->data.reserve(currentStream->headers.size() + frameHeader.payloadLength);
                currentStream->data.insert(currentStream->data.end(),
                                           message.begin() + bytesRead,
                                           message.begin() + bytesRead + frameHeader.payloadLength);
                bytesRead += frameHeader.payloadLength;
                if (frameHeader.flags & 0b00000001) {
                    currentStream->state = RECEIVED_HEADERS_AND_DATA;
                    try {
                        HTTPRequest httpRequest =
                            HTTP::RequestUtils::processHTTPRequest(currentConnection->dynamicTable,
                                                                   currentStream->headers,
                                                                   currentStream->data,
                                                                   epollEvent.data.fd,
                                                                   frameHeader.streamID);
                        executeHandlerForHTTPRequest(httpRequest);
                    } catch (HTTP::RequestUtils::MalformedHTTPRequestException& exception) {
                        length = HTTP::FrameUtils::constructGoAwayFrame(
                            buf, currentConnection->highestStreamID, HTTP::FrameUtils::PROTOCOL_ERROR);
                        writeBuffer(epollEvent.data.fd);
                        closeConnection(currentConnection->ssl, epollEvent.data.fd);
                        // TODO: Replace with sending back HTTP Error Code 400
                        return;
                    }
                }
                break;
            }
            case HTTP::FrameUtils::HEADERS: {

                if (!frameHeader.streamID % 2 || frameHeader.streamID <= currentConnection->highestStreamID) {
                    length = HTTP::FrameUtils::constructGoAwayFrame(
                        buf, currentConnection->highestStreamID, HTTP::FrameUtils::PROTOCOL_ERROR);
                    writeBuffer(epollEvent.data.fd);
                    closeConnection(currentConnection->ssl, epollEvent.data.fd);
                    return;
                }
                currentConnection->highestStreamID = frameHeader.streamID;

                Stream* currentStream = &(currentConnection->streams[frameHeader.streamID]);
                currentStream->compressedHeaderSize = frameHeader.payloadLength;

                bool endStream = frameHeader.flags & 0x1; // END_STREAM (0x1): Flag on Bit 0
                bool endHeaders = frameHeader.flags & 0x4; // END_HEADERS (0x4): Flag on Bit 2

                // Priority flag checking
                if (frameHeader.flags & 0x20) {
                    // TODO: Taking priorities into account
                    bytesRead += 5; // Ignore it for now
                    frameHeader.payloadLength -= 5;
                }

                currentStream->headers.reserve(frameHeader.payloadLength);
                currentStream->headers.assign(message.begin() + bytesRead,
                                              message.begin() + bytesRead + frameHeader.payloadLength);

                bytesRead += frameHeader.payloadLength;

                if (endHeaders && endStream) {
                    // Received Headers without data, stream is half closed now (closed on client side)
                    currentStream->state = RECEIVED_HEADERS;

                    // Process Request
                    try {
                        HTTPRequest httpRequest =
                            HTTP::RequestUtils::processHTTPRequest(currentConnection->dynamicTable,
                                                                   currentStream->headers,
                                                                   epollEvent.data.fd,
                                                                   frameHeader.streamID);
                        executeHandlerForHTTPRequest(httpRequest);
                    } catch (HTTP::RequestUtils::MalformedHTTPRequestException& exception) {
                        length = HTTP::FrameUtils::constructGoAwayFrame(
                            buf, currentConnection->highestStreamID, HTTP::FrameUtils::PROTOCOL_ERROR);
                        writeBuffer(epollEvent.data.fd);
                        closeConnection(currentConnection->ssl, epollEvent.data.fd);
                        // TODO: Replace with sending back HTTP Error Code 400
                        return;
                    }
                } else {

                    //  Scenario one: Headers transmitted, Waiting for Data: StreamStatus should be set to
                    //  RECEIVING_DATA = 2
                    //                endHeaders = true     endStream = false
                    //                    State =   1  +  1 (endHeaders)   =  2
                    //  Scenario two: Headers not transmitted, Waiting for Continuation: StreamStatus should be set
                    //  to RECEIVING_HEADERS = 1
                    //                endHeaders = false     endStream = false
                    //                    State =   1  +  0 (endHeaders)   =  1
                    currentStream->state = static_cast<StreamState>(1 + endHeaders);
                }
                break;
            }
            case HTTP::FrameUtils::PRIORITY:
                bytesRead += frameHeader.payloadLength; // TODO: IMPLEMENT PRIORITY FRAME AND REPLACE THIS LINE
                break;
            case HTTP::FrameUtils::RST_STREAM:
                bytesRead += frameHeader.payloadLength; // TODO: IMPLEMENT RST_STREAM FRAME AND REPLACE THIS LINE
                break;
            case HTTP::FrameUtils::SETTINGS:
                bytesRead += frameHeader.payloadLength; // TODO: IMPLEMENT SETTINGS FRAME AND REPLACE THIS LINE
                break;
            case HTTP::FrameUtils::PUSH_PROMISE:
                bytesRead += frameHeader.payloadLength; // TODO: IMPLEMENT PUSH_PROMISE FRAME AND REPLACE THIS LINE
                break;
            case HTTP::FrameUtils::PING: {
                if (frameHeader.streamID != 0 || frameHeader.payloadLength != 8) {
                    length = HTTP::FrameUtils::constructGoAwayFrame(
                        buf, currentConnection->highestStreamID, HTTP::FrameUtils::PROTOCOL_ERROR);
                    writeBuffer(epollEvent.data.fd);
                    closeConnection(currentConnection->ssl, epollEvent.data.fd);
                    return;
                }
                bool ackFlag = frameHeader.flags & 0b00000001;
                if (!ackFlag) {
                    std::copy(message.begin() + bytesRead, message.begin() + bytesRead + 8, buf.begin() + 9);
                    bytesRead += 8;

                    length = HTTP::FrameUtils::constructAckPingFrame(buf);
                    writeBuffer(epollEvent.data.fd);
                }
                break;
            }
            case HTTP::FrameUtils::GOAWAY:
                bytesRead += frameHeader.payloadLength; // TODO: IMPLEMENT GOAWAY FRAME AND REPLACE THIS LINE
                break;
            case HTTP::FrameUtils::WINDOW_UPDATE:
                bytesRead += frameHeader.payloadLength; // TODO: IMPLEMENT WINDOW_UPDATE FRAME AND REPLACE THIS LINE
                break;
            case HTTP::FrameUtils::CONTINUATION:
                bytesRead += frameHeader.payloadLength; // TODO: IMPLEMENT CONTINUATION FRAME AND REPLACE THIS LINE
                break;
            default:
                // TODO: Add Error message
                throw VulcanoHTTPExceptions::CustomVulcanException<spdlog::level::err>(
                    "There was an error while processing a client message! The server expected a frame header but "
                    "because of a bug or an invalid request the place where it expected contains none. - Error: VULCERROR_NOWAYTOCONTINUE-FRAMETYPE-SWITCH"
                    );
            }

            break;
        }
        case EXPECTING_SETTINGS: {
            HTTP::FrameUtils::FrameHeader frameHeader = HTTP::FrameUtils::interpretFrameHeader(message, bytesRead);

            bytesRead += 9 + frameHeader.payloadLength;
            if (frameHeader.type != HTTP::FrameUtils::SETTINGS || frameHeader.flags != 0) {
                closeConnection(currentConnection->ssl, epollEvent.data.fd);
                return;
            } else if (frameHeader.payloadLength % 6) {
                length = HTTP::FrameUtils::constructGoAwayFrame(buf, 0, HTTP::FrameUtils::FRAME_SIZE_ERROR);
                writeBuffer(epollEvent.data.fd);
                closeConnection(currentConnection->ssl, epollEvent.data.fd);
                return;
            } else if (frameHeader.streamID != 0) {
                length = HTTP::FrameUtils::constructGoAwayFrame(buf, 0, HTTP::FrameUtils::PROTOCOL_ERROR);
                writeBuffer(epollEvent.data.fd);
                closeConnection(currentConnection->ssl, epollEvent.data.fd);
                return;
            }
            // TODO: Check and Save Client Settings
            length = HTTP::FrameUtils::constructSettingsAck(buf);
            writeBuffer(epollEvent.data.fd);

            currentConnection->state = CONNECTION_PREFACE_FINISHED;

            break;
        }
        case WAITING_FOR_MAGIC: {
            std::string magic(24, '\0');
            std::copy(message.begin() + bytesRead, message.begin() + bytesRead + 24, magic.begin());
            bytesRead += 24;

            if (magic != "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n") {
                SSL_shutdown(currentConnection->ssl);
                close(epollEvent.data.fd);
                return;
            }

            // TODO: Make server settings settable
            char serverSettingsBuf[] = {0,    0, 0x12, 4, 0,    0, 0, 0, 0, 0, 3, 0, 0, 0,
                                        0x64, 0, 4,    0, 0x10, 0, 0, 0, 6, 0, 1, 0, 0};
            SSL_write(currentConnection->ssl, serverSettingsBuf, 27);

            currentConnection->state = EXPECTING_SETTINGS;
        }
        }
    }
}
void HTTPServer::onClose(epoll_event epollEvent)
{
    closeConnection(connections->operator[](epollEvent.data.fd).ssl, epollEvent.data.fd);
    connections->erase(epollEvent.data.fd);
}
[[maybe_unused]] void HTTPServer::start()
{
    EventBasedTCPSocket::start(listenOn);
}
void HTTPServer::writeBuffer(int fd)
{
    // TODO: ERROR HANDLING
    SSL_write(connections->operator[](fd).ssl, buf.data(), length);
}
void HTTPServer::addRequestHandler(std::regex& regex, const std::function<void(const HTTPRequest&)>& handler)
{
    requestHandlers.emplace_back(regex, handler);
}
// The path of the HTTP requests is matched against the regexes. The first one which matches the corresponding handler
// will be executed. It is important to note that the regex is matched rawly, so if you have a regex "/test" the path
// "/abc/test" would match it. To make sure only the exact string will be matched, you should use '^' character at the
// start and the '$' character at the end of your regex or use the add addRequestHandlerSimpleString when you don't need
// to use a regex. The following regex wouldn't match "/abc/test" anymore and only "/test":
//     "^/test$"
// IMPORTANT: you must ensure sure you escape everything in your regex right. That means that when you use a '\" to
// escape something in the regex, you have to escape the '\' with again in your source code, so you would for example
// write:
//     "^/picture\\.png$",
// if your actual regex is "^/picture\.png$" (in regex you have to escape '.' characters)
void HTTPServer::addRequestHandler(const std::string& regex, const std::function<void(const HTTPRequest&)>& handler)
{
    requestHandlers.emplace_back(std::regex(regex), handler);
}

// TODO:
// Accepts a simple string which the path exactly has to match up with.
// void HTTPServer::addRequestHandlerSimpleString(const std::string& string, const std::function<void(HTTPRequest&)>&
// handler)
//{
//    requestHandlers.emplace_back(std::regex(string), handler);
//}

// Removes any request handler of which the corresponding regex matches the specified string.
[[maybe_unused]] void HTTPServer::removeRequestHandlers(const std::string& string)
{
    erase_if(requestHandlers, [&string](auto handlerPair) { return std::regex_search(string, handlerPair.first); });
}
const std::unique_ptr<std::unordered_map<int, HTTPServer::Connection>>& HTTPServer::getConnections()
{
    return connections;
}