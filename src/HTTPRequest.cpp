/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HTTPRequest.h"
#include "HTTPRequestResponseCommon.h"
#include "HTTPRequestUtils.h"
#include <algorithm>
#include <string>

bool HTTPRequest::hasAllNecessaryHeaderFields() const
{
    return std::all_of(necessaryHeaderFields.begin(), necessaryHeaderFields.end(), [](bool b) { return b; });
}
void HTTPRequest::addToHeaderList(std::vector<HTTP::RequestUtils::headerField_t>& dynamicTable,
                                  const unsigned int index)
{
    using HTTP::RequestUtils::staticTable;

    if (index < staticTable.size()) {
        // Index is in static table range
        addToHeaderList(staticTable[index]);
    } else if (index < staticTable.size() + dynamicTable.size()) {
        // Index is in dynamic table range
        addToHeaderList(
            dynamicTable[dynamicTable.size() - 1
                         - (index - staticTable.size())]); // Values are stored backwards in dynamic table vector, so
                                                           // the lowest index, as used in HTTP, leads to the highest
                                                           // index in the dynamic table vector
    } else {
        // Index is not in dynamic table range or in static table range
        throw HTTP::RequestUtils::MalformedHTTPHeaderFieldException();
    }
}
void HTTPRequest::addToHeaderList(std::vector<HTTP::RequestUtils::headerField_t>& dynamicTable,
                                  const unsigned int index,
                                  const std::string& headerFieldValue)
{
    HTTP::RequestUtils::headerField_t addedHeaderField("", headerFieldValue);
    if (index < HTTP::RequestUtils::staticTable.size()) {
        // Index is in static table range
        addedHeaderField.first = HTTP::RequestUtils::staticTable[index].first;
    } else if (index < HTTP::RequestUtils::staticTable.size() + dynamicTable.size()) {
        // Index is in dynamic table range
        addedHeaderField.first =
            dynamicTable[dynamicTable.size() - 1 - (index - HTTP::RequestUtils::staticTable.size())].first;
    } else {
        // Index is not in dynamic table range or in static table range
        throw HTTP::RequestUtils::MalformedHTTPHeaderFieldException();
    }
    addToHeaderList(addedHeaderField);
}
void HTTPRequest::addToHeaderListAndDynamicTable(std::vector<HTTP::RequestUtils::headerField_t>& dynamicTable,
                                                 const unsigned int index,
                                                 const std::string& headerFieldValue)
{
    HTTP::RequestUtils::headerField_t addedHeaderField("", headerFieldValue);
    if (index < HTTP::RequestUtils::staticTable.size()) {
        // Index is in static table range
        addedHeaderField.first = HTTP::RequestUtils::staticTable[index].first;
    } else if (index < HTTP::RequestUtils::staticTable.size() + dynamicTable.size()) {
        // Index is in dynamic table range
        addedHeaderField.first =
            dynamicTable[dynamicTable.size() - 1 - (index - HTTP::RequestUtils::staticTable.size())].first;
    } else {
        // Index is not in dynamic table range or in static table range
        throw HTTP::RequestUtils::MalformedHTTPHeaderFieldException();
    }
    dynamicTable.push_back(addedHeaderField);

    addToHeaderList(addedHeaderField);
}
void HTTPRequest::addToHeaderList(const std::string& headerFieldKey, const std::string& headerFieldValue)
{
    HTTP::RequestUtils::headerField_t addedHeaderField =
        HTTP::RequestUtils::headerField_t(headerFieldKey, headerFieldValue);
    addToHeaderList(addedHeaderField);
}
void HTTPRequest::addToHeaderListAndDynamicTable(std::vector<HTTP::RequestUtils::headerField_t>& dynamicTable,
                                                 const std::string& headerFieldKey,
                                                 const std::string& headerFieldValue)
{
    HTTP::RequestUtils::headerField_t addedHeaderField =
        HTTP::RequestUtils::headerField_t(headerFieldKey, headerFieldValue);
    dynamicTable.push_back(addedHeaderField);

    addToHeaderList(addedHeaderField);
}
void HTTPRequest::addToHeaderList(const HTTP::RequestUtils::headerField_t& headerField)
{
    headerList.push_back(headerField);
    using HTTP::RequestUtils::MalformedHTTPRequestException;
    if (headerField.first == ":method") {
        if (necessaryHeaderFields[0]) {
            throw MalformedHTTPRequestException();
        } else {
            necessaryHeaderFields[0] = true;
        }
    } else if (headerField.first == ":scheme") {
        if (necessaryHeaderFields[1]) {
            throw MalformedHTTPRequestException();
        } else {
            necessaryHeaderFields[1] = true;
        }
    } else if (headerField.first == ":path") {
        if (necessaryHeaderFields[2]) {
            throw MalformedHTTPRequestException();
        } else {
            necessaryHeaderFields[2] = true;
        }
    } else if (headerField.first == ":authority") {
        if (necessaryHeaderFields[3]) {
            throw MalformedHTTPRequestException();
        } else {
            necessaryHeaderFields[3] = true;
        }
    }
}
const std::vector<HTTP::RequestUtils::headerField_t>& HTTPRequest::getDecodedHeaderList() const
{
    return headerList;
}
const HTTP::RequestUtils::headerField_t& HTTPRequest::getHeaderFieldFromKey(std::string_view key) const
{
    return *std::find_if(
        headerList.begin(), headerList.end(), [&key](auto headerField) { return headerField.first == key; });
}
HTTPRequest::HTTPRequest(int clientFileDescriptor, uint32_t streamID)
    : HTTPRequestResponseCommon(clientFileDescriptor, streamID)
{
}
