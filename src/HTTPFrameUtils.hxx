/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HTTPBinaryRepresentationUtils.h"

template <typename T>
void
HTTP::FrameUtils::constructFrameHeader(T& buffer, uint32_t payloadLength, FrameType type, unsigned char flags, uint32_t streamID) {

    //                       Frame Header
    //     +-----------------------------------------------+
    //    |                 Length (24)                   |
    //    +---------------+---------------+---------------+
    //    |   Type (8)    |   Flags (8)   |
    //    +-+-------------+---------------+-------------------------------+
    //    |R|                 Stream Identifier (31)                      |
    //    +=+=============================================================+
    //    |                   Frame Payload (0...)                      ...
    //    +---------------------------------------------------------------+

    buffer[0] = (payloadLength >> 16) & 0xFF;
    buffer[1] = (payloadLength >> 8) & 0xFF;
    buffer[2] = payloadLength & 0xFF;

    buffer[3] = type;

    buffer[4] = flags;

    buffer[5] = (streamID >> 24) & 0xFF;
    buffer[6] = (streamID >> 16) & 0xFF;
    buffer[7] = (streamID >> 8) & 0xFF;
    buffer[8] = streamID & 0xFF;
}
template <typename T>
void HTTP::FrameUtils::constructFrameHeader(T& buffer,
                          uint32_t payloadLength,
                          HTTP::FrameUtils::FrameType type,
                          unsigned char flags,
                          uint32_t streamID,
                          unsigned int offset) {
    //                       Frame Header
    //     +-----------------------------------------------+
    //    |                 Length (24)                   |
    //    +---------------+---------------+---------------+
    //    |   Type (8)    |   Flags (8)   |
    //    +-+-------------+---------------+-------------------------------+
    //    |R|                 Stream Identifier (31)                      |
    //    +=+=============================================================+
    //    |                   Frame Payload (0...)                      ...
    //    +---------------------------------------------------------------+

    buffer[offset + 0] = (payloadLength >> 16) & 0xFF;
    buffer[offset + 1] = (payloadLength >> 8) & 0xFF;
    buffer[offset + 2] = payloadLength & 0xFF;

    buffer[offset + 3] = type;

    buffer[offset + 4] = flags;

    buffer[offset + 5] = (streamID >> 24) & 0xFF;
    buffer[offset + 6] = (streamID >> 16) & 0xFF;
    buffer[offset + 7] = (streamID >> 8) & 0xFF;
    buffer[offset + 8] = streamID & 0xFF;
}
template <bool huffmanEncode, typename T>
unsigned int HTTP::FrameUtils::writeHeaderListToBuffer(unsigned int bytesWritten,
                                     const std::vector<HTTP::FrameUtils::HTTPHeaderField>& headerList,
                                     T& buffer) {

    for (const HTTPHeaderField& headerField : headerList) {
        switch (headerField.type) {
        case INDEXED:
            bytesWritten += HTTP::BinaryRepresentationUtils::constructIntegerRepresentation(
                7, 0b10000000, headerField.index, bytesWritten, buffer);
            break;
        case LITERAL_INDEXED:
            bytesWritten += HTTP::BinaryRepresentationUtils::constructIntegerRepresentation(
                6, 0b01000000, headerField.index, bytesWritten, buffer);
            bytesWritten += HTTP::BinaryRepresentationUtils::constructStringRepresentation<huffmanEncode>(
                headerField.pair.second, bytesWritten, buffer);
            break;
        case LITERAL_INDEXED_NEW_NAME:
            bytesWritten++;
            bytesWritten += HTTP::BinaryRepresentationUtils::constructStringRepresentation<huffmanEncode>(
                0b01000000, headerField.pair.first, bytesWritten, buffer);
            bytesWritten += HTTP::BinaryRepresentationUtils::constructStringRepresentation<huffmanEncode>(
                headerField.pair.second, bytesWritten, buffer);
            break;
        case LITERAL_NOT_INDEXED:
            bytesWritten += HTTP::BinaryRepresentationUtils::constructIntegerRepresentation(
                4, 0, headerField.index, bytesWritten, buffer);
            bytesWritten += HTTP::BinaryRepresentationUtils::constructStringRepresentation<huffmanEncode>(
                headerField.pair.second, bytesWritten, buffer);
            break;
        case LITERAL_NOT_INDEXED_NEW_NAME:
            bytesWritten++;
            bytesWritten += HTTP::BinaryRepresentationUtils::constructStringRepresentation<huffmanEncode>(
                0, headerField.pair.first, bytesWritten, buffer);
            bytesWritten += HTTP::BinaryRepresentationUtils::constructStringRepresentation<huffmanEncode>(
                headerField.pair.second, bytesWritten, buffer);
            break;
        case LITERAL_NEVER_INDEXED:
            bytesWritten += HTTP::BinaryRepresentationUtils::constructIntegerRepresentation(
                4, 0b00010000, headerField.index, bytesWritten, buffer);
            bytesWritten += HTTP::BinaryRepresentationUtils::constructStringRepresentation<huffmanEncode>(
                headerField.pair.second, bytesWritten, buffer);
            break;
        case LITERAL_NEVER_INDEXED_NEW_NAME:
            bytesWritten++;
            bytesWritten += HTTP::BinaryRepresentationUtils::constructStringRepresentation<huffmanEncode>(
                0b00010000, headerField.pair.first, bytesWritten, buffer);
            bytesWritten += HTTP::BinaryRepresentationUtils::constructStringRepresentation<huffmanEncode>(
                headerField.pair.second, bytesWritten, buffer);
            break;
        }
    }
    return bytesWritten;
}
