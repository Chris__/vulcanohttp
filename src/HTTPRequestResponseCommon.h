/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_HTTPREQUESTRESPONSECOMMON_H
#define VULCANOHTTP_HTTPREQUESTRESPONSECOMMON_H

#include <string>
#include <vector>

namespace HTTP::RequestUtils
{
    typedef std::pair<std::string, std::string> headerField_t;
}
class HTTPRequestResponseCommon
{
protected:
public:

    int clientFileDescriptor;

    uint32_t streamID;

    HTTPRequestResponseCommon(int clientFileDescriptor, uint32_t streamID);
};

#endif // VULCANOHTTP_HTTPREQUESTRESPONSECOMMON_H
