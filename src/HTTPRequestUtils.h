/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_HTTPREQUESTUTILS_H
#define VULCANOHTTP_HTTPREQUESTUTILS_H

#include "HTTPRequest.h"
#include <array>
#include <exception>
#include <string>
#include <unordered_map>
#include <utility>
namespace HTTP::RequestUtils
{
    typedef std::pair<std::string, std::string> headerField_t;

    // Exceptions
    struct MalformedHTTPRequestException : public std::exception
    {
        [[nodiscard]] const char* what() const noexcept override
        {
            return "A malformed HTTP Request was received";
        }
    };
    struct MalformedHTTPHeaderFieldException : public MalformedHTTPRequestException
    {
        [[nodiscard]] const char* what() const noexcept override
        {
            return "A client sent a HTTP Request with a HTTP header field index which is not known or can't exist";
        }
    };

    // Enums
    enum HTTPMethod
    {
        GET,
        POST,
        PUT,
        DELETE,
        OPTIONS,
        HEAD,
        TRACE,
        PATCH
    };

    // Variables
    const std::unordered_map<std::string, HTTPMethod> httpMethodEnumNames = {{"GET", HTTPMethod::GET},
                                                                             {"POST", HTTPMethod::POST},
                                                                             {"PUT", HTTPMethod::PUT},
                                                                             {"DELETE", HTTPMethod::DELETE},
                                                                             {"OPTIONS", HTTPMethod::OPTIONS},
                                                                             {"HEAD", HTTPMethod::HEAD},
                                                                             {"TRACE", HTTPMethod::TRACE},
                                                                             {"PATCH", HTTPMethod::PATCH}};
    const std::array<headerField_t, 62> staticTable = {
        headerField_t("NOFIELD", "NOFIELD"),
        headerField_t(":authority", ""),
        headerField_t(":method", "GET"),
        headerField_t(":method", "POST"),
        headerField_t(":path", "/"),
        headerField_t(":path", "/index.html"),
        headerField_t(":scheme", "http"),
        headerField_t(":scheme", "https"),
        headerField_t(":status", "200"),
        headerField_t(":status", "204"),
        headerField_t(":status", "206"),
        headerField_t(":status", "304"),
        headerField_t(":status", "400"),
        headerField_t(":status", "404"),
        headerField_t(":status", "500"),
        headerField_t("accept-charset", ""),
        headerField_t("accept-encoding", "gzip, deflate"),
        headerField_t("accept-language", ""),
        headerField_t("accept-ranges", ""),
        headerField_t("accept", ""),
        headerField_t("access-control-allow-origin", ""),
        headerField_t("age", ""),
        headerField_t("allow", ""),
        headerField_t("authorization", ""),
        headerField_t("cache-control", ""),
        headerField_t("content-disposition", ""),
        headerField_t("content-encoding", ""),
        headerField_t("content-language", ""),
        headerField_t("content-length", ""),
        headerField_t("content-location", ""),
        headerField_t("content-range", ""),
        headerField_t("content-type", ""),
        headerField_t("cookie", ""),
        headerField_t("date", ""),
        headerField_t("etag", ""),
        headerField_t("expect", ""),
        headerField_t("expires", ""),
        headerField_t("from", ""),
        headerField_t("host", ""),
        headerField_t("if-match", ""),
        headerField_t("if-modified-since", ""),
        headerField_t("if-none-match", ""),
        headerField_t("if-range", ""),
        headerField_t("if-unmodified-since", ""),
        headerField_t("last-modified", ""),
        headerField_t("link", ""),
        headerField_t("location", ""),
        headerField_t("max-forwards", ""),
        headerField_t("proxy-authenticate", ""),
        headerField_t("proxy-authorization", ""),
        headerField_t("range", ""),
        headerField_t("referer", ""),
        headerField_t("refresh", ""),
        headerField_t("retry-after", ""),
        headerField_t("server", ""),
        headerField_t("set-cookie", ""),
        headerField_t("strict-transport-security", ""),
        headerField_t("transfer-encoding", ""),
        headerField_t("user-agent", ""),
        headerField_t("vary", ""),
        headerField_t("via", ""),
        headerField_t("www-authenticate", ""),
    };

    HTTPRequest processHTTPRequest(std::vector<headerField_t>& dynamicTable,
                                   std::vector<unsigned char>& headers,
                                   int clientFileDescriptor,
                                   uint32_t streamID);
    HTTPRequest processHTTPRequest(std::vector<headerField_t>& dynamicTable,
                                   std::vector<unsigned char>& headers,
                                   std::vector<unsigned char>& data,
                                   int clientFileDescriptor,
                                   uint32_t streamID);
}; // namespace HTTP::RequestUtils

#endif // VULCANOHTTP_HTTPREQUESTUTILS_H