/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HTTPRequestUtils.h"
#include "HTTPBinaryRepresentationUtils.h"
#include "HTTPRequest.h"
#include <array>
#include <iostream>

// The headers are the still-compressed payloads of the original HEADERS frame and all CONTINUATION FRAMES
// The data is the payload of all DATA frames sent after the HEADERS frame before half-closing the stream
//
//  Processing in that case means, decompressing the headers and formatting the whole request in an easy-to-access
//  format (i.e. HTTPRequest struct)
HTTPRequest HTTP::RequestUtils::processHTTPRequest(std::vector<headerField_t>& dynamicTable,
                                                   std::vector<unsigned char>& headers,
                                                   int clientFileDescriptor,
                                                   uint32_t streamID)
{
    unsigned int bytesRead = 0;
    unsigned int headersLength = headers.size();

    HTTPRequest resultHTTPRequest(clientFileDescriptor, streamID);

    {

        while (bytesRead < headersLength) {
            // Indexed Header Field Representation:
            if (headers[bytesRead] >> 7) {
                unsigned int index =
                    HTTP::BinaryRepresentationUtils::interpretIntegerRepresentation(headers, bytesRead, 7);
                if (index == 0) {
                    throw MalformedHTTPHeaderFieldException();
                }
                resultHTTPRequest.addToHeaderList(dynamicTable, index);
            } else {
                // Literal Header Field with Incremental Indexing:
                if (headers[bytesRead] >> 6) {
                    unsigned int index =
                        HTTP::BinaryRepresentationUtils::interpretIntegerRepresentation(headers, bytesRead, 6);
                    if (index == 0) {
                        // Literal Header Field with Incremental Indexing -- New Name:
                        std::string headerFieldKey =
                            HTTP::BinaryRepresentationUtils::interpretStringRepresentation(headers, bytesRead);
                        std::string headerFieldValue =
                            HTTP::BinaryRepresentationUtils::interpretStringRepresentation(headers, bytesRead);
                        resultHTTPRequest.addToHeaderListAndDynamicTable(
                            dynamicTable, headerFieldKey, headerFieldValue);
                    } else {
                        // Literal Header Field with Incremental Indexing -- Indexed Name:
                        std::string headerFieldValue =
                            HTTP::BinaryRepresentationUtils::interpretStringRepresentation(headers, bytesRead);
                        resultHTTPRequest.addToHeaderListAndDynamicTable(dynamicTable, index, headerFieldValue);
                    }
                } else {
                    // Dynamic Table Size Update:
                    if (headers[bytesRead] >> 5) {
                        HTTP::BinaryRepresentationUtils::interpretIntegerRepresentation(headers, bytesRead, 5);
                        // TODO: Implement Dynamic Table Size Update
                    } else {
                        // Literal Header Field Never Indexed and Literal Header Field without Indexing:
                        // As this server is no intermediary, the implementation of both is the same

                        unsigned int index =
                            HTTP::BinaryRepresentationUtils::interpretIntegerRepresentation(headers, bytesRead, 4);
                        if (index == 0) {
                            // Literal Header Field with Incremental Indexing -- New Name:
                            std::string headerFieldKey =
                                HTTP::BinaryRepresentationUtils::interpretStringRepresentation(headers, bytesRead);
                            std::string headerFieldValue =
                                HTTP::BinaryRepresentationUtils::interpretStringRepresentation(headers, bytesRead);
                            resultHTTPRequest.addToHeaderList(headerFieldKey, headerFieldValue);
                        } else {
                            // Literal Header Field with Incremental Indexing -- Indexed Name:
                            std::string headerFieldValue =
                                HTTP::BinaryRepresentationUtils::interpretStringRepresentation(headers, bytesRead);
                            resultHTTPRequest.addToHeaderList(dynamicTable, index, headerFieldValue);
                        }
                    }
                }
            }
        }
        if (!resultHTTPRequest.hasAllNecessaryHeaderFields()) {
            throw MalformedHTTPRequestException();
        }
    }
    return resultHTTPRequest;
}
HTTPRequest HTTP::RequestUtils::processHTTPRequest(std::vector<headerField_t>& dynamicTable,
                                                   std::vector<unsigned char>& headers,
                                                   std::vector<unsigned char>& data,
                                                   int clientFileDescriptor,
                                                   uint32_t streamID)
{
    HTTPRequest result = processHTTPRequest(dynamicTable, headers, clientFileDescriptor, streamID);
    result.bodyData = std::move(data);
    return result;
}