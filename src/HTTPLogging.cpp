//
// Created by christian on 7/29/22.
//
#include "ConsoleLogSink.h"
#include <vulcanohttp/HTTPLogging.h>
std::optional<spdlog::logger> HTTP::Logging::logger;
void HTTP::Logging::setLogger(spdlog::logger newLogger){
    logger = std::move(newLogger);
}
void HTTP::Logging::initializeVulcanoHTTPLogger() {
    if (!HTTP::Logging::logger.has_value()) {
        HTTP::Logging::setLogger(
            spdlog::logger("VulcanoHTTP", std::make_shared<ConsoleLogSink_mt>(spdlog::level::err)));
        HTTP::Logging::logger->set_level(spdlog::level::trace);
        VULCANOHTTP_LOG(debug, "Automatically initialized Logger");
    }
}