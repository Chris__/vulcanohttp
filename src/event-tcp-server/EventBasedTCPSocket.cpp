/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EventBasedTCPSocket.h"
#include "../ConsoleLogSink.h"
#include <cstring>
#include <vulcanohttp/HTTPLogging.h>
#include <spdlog/spdlog.h>
#include <thread>
#include <unistd.h>

EventBasedTCPSocket::~EventBasedTCPSocket()
{
    if (started) {
        stop();
    }
}
void EventBasedTCPSocket::start(const std::vector<listeningBindingPair_t>& listenOn)
{
    started = true;

    std::thread localEpollLooperThread(&EpollLooper::start, &epollLooper, this, listenOn);

    epollLooperThread = std::move(localEpollLooperThread);
}

void EventBasedTCPSocket::stop()
{
    started = false;

    VULCANOHTTP_LOG(info,"Shutting down EpollLoooper! This can take a while.");
    epollLooper.stop();
    epollLooperThread.join();
}
int EventBasedTCPSocket::customRead(int fd, std::array<char, 4097>& buffer)
{
    return read(fd, buffer.data(), 4096);
}
bool EventBasedTCPSocket::onOpen(int fd)
{
    return true;
}
void EventBasedTCPSocket::onClose(epoll_event epollEvent)
{
}
void EventBasedTCPSocket::onMessage(epoll_event epollEvent, std::vector<char>& message)
{
}
