/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_EPOLLLOOPER_H
#define VULCANOHTTP_EPOLLLOOPER_H

#include <functional>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <variant>

class EventBasedTCPSocket;

typedef std::pair<std::variant<in_addr_t, in6_addr>,uint16_t> listeningBindingPair_t;
enum EpollFileDescriptorRoles {
    PIPE = 0,
    LISTENINGFD,
    CONNECTEDFD
};
class EpollLooper
{
public:
    EpollLooper() = default;
    ~EpollLooper();

    bool start(EventBasedTCPSocket* ebTcpSocket, const std::vector<listeningBindingPair_t>& listenOn);
    void stop();

    void addFileDescriptor(int fd, EpollFileDescriptorRoles role) const;
    void removeFileDescriptor(int fd, EpollFileDescriptorRoles role) const;

private:
    std::array<int, 2> pipeFileDescriptors{};
    int epollFileDescriptor;
    std::vector<int> listeningFileDescriptors;
    socklen_t sockLen;

    bool started = false;
    void setupListeningSocket(const listeningBindingPair_t& bindAddressPortPair);
};

#endif // VULCANOHTTP_EPOLLLOOPER_H
