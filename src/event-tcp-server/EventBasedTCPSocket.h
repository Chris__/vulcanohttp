/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULCANOHTTP_EVENTBASEDTCPSOCKET_H
#define VULCANOHTTP_EVENTBASEDTCPSOCKET_H

#include "EpollLooper.h"
#include <functional>
#include <sys/epoll.h>
#include <thread>

class EventBasedTCPSocket
{
public:
    ~EventBasedTCPSocket();
    void start(const std::vector<listeningBindingPair_t>& listenOn);

    void stop();

    virtual int customRead(int fd,
                           std::array<char, 4097>& buffer); // std::function<int(int, std::array<char, 4097>&)> read;
    virtual bool onOpen(int fd); // std::function<bool(int)> onOpen;
    virtual void onClose(epoll_event epollEvent); // std::function<void(epoll_event)> onClose;
    virtual void
    onMessage(epoll_event epollEvent,
              std::vector<char>& message); // std::function<void(epoll_event, std::vector<char>&)> onMessage;

private:
    bool started = false;

    EpollLooper epollLooper;

    std::thread epollLooperThread;
};

#endif // VULCANOHTTP_EVENTBASEDTCPSOCKET_H
