/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EpollLooper.h"
#include "../VulcanoHTTPExceptions.h"
#include "EventBasedTCPSocket.h"
#include <arpa/inet.h>
#include <cstring>
#include <fcntl.h>
#include <functional>
#include <ifaddrs.h>
#include <net/if.h>
#include <netdb.h>
#include <sys/epoll.h>
#include <unistd.h>

std::optional<uint32_t> getScopeIdForIp(const std::array<char, 41>& ip)
{
    ifaddrs* networkInterfaces;
    std::array<char, 41> ipAddress{};
    uint32_t scopeId = 0;
    getifaddrs(&networkInterfaces);
    for (ifaddrs* currentInterface = networkInterfaces; currentInterface;
         currentInterface = currentInterface->ifa_next) {
        if (currentInterface->ifa_addr
            && currentInterface->ifa_addr->sa_family == AF_INET6) { // only interested in ipv6 ones
            getnameinfo(currentInterface->ifa_addr,
                        sizeof(sockaddr_in6),
                        ipAddress.data(),
                        sizeof(ipAddress),
                        nullptr,
                        0,
                        NI_NUMERICHOST);
            int i = 0;
            for (; ipAddress[i]; i++) {
                if (ipAddress[i] == '%') {
                    ipAddress[i] = '\0';
                    break;
                }
            }
            // if the ip matches, convert the interface name to a scope index
            if (std::equal(ipAddress.begin(), ipAddress.begin() + i - 1, ip.begin())) {
                scopeId = if_nametoindex(currentInterface->ifa_name);
                freeifaddrs(networkInterfaces);
                return scopeId;
            }
            bzero(ipAddress.data(), ipAddress.size());
        }
    }
    freeifaddrs(networkInterfaces);
    return {};
}
int reuseaddr = 1;
void EpollLooper::setupListeningSocket(const listeningBindingPair_t& bindAddressPortPair)
{
    const int protocolFamily = bindAddressPortPair.first.index() ? AF_INET6 : AF_INET;
    addrinfo hints = {.ai_flags = AI_NUMERICHOST,
                      .ai_family = protocolFamily,
                      .ai_socktype = SOCK_STREAM,
                      .ai_protocol = IPPROTO_TCP};

    VULCANOHTTP_LOG(debug, "Protocol/Address family: {}", protocolFamily);

    int newListeningFileDescriptor = socket(protocolFamily, SOCK_STREAM, 0);
    if (newListeningFileDescriptor < 0)
        throw VulcanoHTTPExceptions::ErrnoException<spdlog::level::critical>(
            "There was an error while opening a socket!");
    setsockopt(newListeningFileDescriptor, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr));

    fcntl(newListeningFileDescriptor, F_SETFL, O_NONBLOCK);

    int error = 1;
    std::array<char, 41> arr{};
    socklen_t sockaddrSize;

    std::variant<sockaddr_in6, sockaddr_in, sockaddr> bindAddress;

    std::visit([&arr, &protocolFamily, &bindAddressPortPair](
                   const auto& addr) { inet_ntop(protocolFamily, &(addr), arr.data(), 41); },
               bindAddressPortPair.first);

    if (protocolFamily == AF_INET) {

        bindAddress = sockaddr_in{.sin_family = static_cast<sa_family_t>(protocolFamily),
                                  .sin_port = htons(bindAddressPortPair.second),
                                  .sin_addr = {.s_addr = std::get<in_addr_t>(bindAddressPortPair.first)},
                                  .sin_zero = {0, 0, 0, 0, 0, 0, 0, 0}};

        sockaddrSize = sizeof(sockaddr_in);
    } else {
        // inet_ntop(protocolFamily, &(std::get<in6_addr>(bindAddressPortPair.first)), arr.data(), 41);
        bindAddress = sockaddr_in6{.sin6_family = static_cast<sa_family_t>(protocolFamily),
                                   .sin6_port = htons(bindAddressPortPair.second),
                                   .sin6_flowinfo = 0,
                                   .sin6_addr = std::get<in6_addr>(bindAddressPortPair.first),
                                   .sin6_scope_id = getScopeIdForIp(arr).value()};
        sockaddrSize = sizeof(sockaddr_in6);
    }

    std::visit(
        [&newListeningFileDescriptor, &error, &sockaddrSize](const auto& bindAddress) {
            error = bind(newListeningFileDescriptor, reinterpret_cast<const sockaddr*>(&bindAddress), sockaddrSize);
        },
        bindAddress);
    using namespace std::string_literals;
    if (error != 0) {
        throw VulcanoHTTPExceptions::ErrnoException<spdlog::level::critical>(
            fmt::format("Couldn't bind to address and port combination {}:{}. Maybe the programme hasn't got "
                        "permission to open a socket on this port "
                        "or the port is already in use!",
                        arr.data(),
                        bindAddressPortPair.second));
    }
    error = listen(newListeningFileDescriptor, 50);
    if (error != 0)
        throw VulcanoHTTPExceptions::ErrnoException<spdlog::level::critical>(
            fmt::format("Couldn't listen on address and port combination {}:{}. Maybe the programme hasn't got "
                        "permission to open a socket on this port "
                        "or the port is already in use!",
                        arr.data(),
                        bindAddressPortPair.second));
    listeningFileDescriptors.push_back(newListeningFileDescriptor);
    VULCANOHTTP_LOG(info, "Listening on port-address combination {}:{}", arr.data(), bindAddressPortPair.second);
}
bool EpollLooper::start(EventBasedTCPSocket* ebTcpSocket, const std::vector<listeningBindingPair_t>& listenOn)
{
    if (started) {
        spdlog::warn("On an EpollLooper start() was run, altouhh already started!");
        return false;
    }
    started = true;

    for (const listeningBindingPair_t& bindAddressPortPair : listenOn) {
        VULCANOHTTP_LOG(debug,
                        "Setting up a listening socket for port {}, corresponding bindAddressPortPair stored at {}",
                        bindAddressPortPair.second,
                        (void*)&bindAddressPortPair);
        setupListeningSocket(bindAddressPortPair);
    }

    epollFileDescriptor = epoll_create1(0);
    if (epollFileDescriptor == -1) {
        throw VulcanoHTTPExceptions::ErrnoException<spdlog::level::critical>("Failed to create epoll file descriptor");
    } else if (pipe(pipeFileDescriptors.data()) != 0) { // Using pipes to communicate to EpollLooper from another
                                                        // thread that it should shut down.
        throw VulcanoHTTPExceptions::ErrnoException<spdlog::level::critical>(
            "Error while creating a pipe in the EpollLooper for interprocess communication!");
    }
    // Listen for events on following file descriptors
    addFileDescriptor(pipeFileDescriptors[0], EpollFileDescriptorRoles::PIPE);
    for (int listeningFileDescriptor : listeningFileDescriptors) {
        addFileDescriptor(listeningFileDescriptor, EpollFileDescriptorRoles::LISTENINGFD);
    }

    size_t bytesRead;
    std::array<char, 4097> readBuffer{};
    struct epoll_event events[200];

    int eventCount;
    int i;

    sockaddr_in clientAddress{};
    sockLen = sizeof(clientAddress);
    int newConnection;

    while (started) {
        EpollFileDescriptorRoles fdRole;
        eventCount = epoll_wait(epollFileDescriptor, events, 200, 30000);
        for (i = 0; i < eventCount; i++) {
            fdRole = static_cast<EpollFileDescriptorRoles>(events[i].data.u64 >> 32);
            if (fdRole == PIPE && read(events[i].data.fd, readBuffer.data(), 1) == 1
                && readBuffer[0] == 1) { // Special Case: Internal communication through pipe, this is being used here
                                         // as a signal for EpollLooper to shut down
                close(pipeFileDescriptors[0]);
                close(pipeFileDescriptors[1]);
                return true;
            } else if (fdRole == LISTENINGFD) {
                newConnection = accept(events[i].data.fd, (struct sockaddr*)&clientAddress, &sockLen);
                if (newConnection != -1 && ebTcpSocket->onOpen(newConnection)) {
                    addFileDescriptor(newConnection, CONNECTEDFD);
                } else {
                    VULCANOHTTP_ERRNOLOG(warn, "Accepting a new peer connection wasn't successful - Error");
                }
            } else {
                bytesRead = ebTcpSocket->customRead(events[i].data.fd, readBuffer);
                if (bytesRead == 0) {
                    // SPDLOG_LOGGER_INFO(HTTP::Logging::logger,"Peer shutdown connection!");
                    removeFileDescriptor(events[i].data.fd, CONNECTEDFD);

                    ebTcpSocket->onClose(events[i]);

                    continue;
                }
                readBuffer[bytesRead] = '\0';

                std::vector<char> receivedMessage(readBuffer.begin(), readBuffer.begin() + bytesRead);
                ebTcpSocket->onMessage(events[i], receivedMessage);
            }
        }
    }
    VULCANOHTTP_LOG(info, "EpollLooper shutdown!");
    return true;
}

void EpollLooper::stop()
{
    if (started) {
        char buffer = 1;
        if (write(pipeFileDescriptors[1], &buffer, 1) != 1) {
            VULCANOHTTP_LOG(warn,
                "An error occured while trying to use a pipe to signal the EpollLooper to shutdown. "
                                        "The message couldn't be written to the pipe  - Error: {}", std::strerror(errno));
        }
        close(epollFileDescriptor);
        started = false;
    }
}
void EpollLooper::addFileDescriptor(int fd, EpollFileDescriptorRoles role) const
{
    epoll_event event{.events = EPOLLIN,
                      .data = {.u64 = static_cast<uint64_t>(fd) | static_cast<uint64_t>(role) << 32}};

    if (!epoll_ctl(epollFileDescriptor, EPOLL_CTL_ADD, fd, &event)) {
        VULCANOHTTP_LOG(trace, "New file descriptor successfully added to EpollLooper!");
    } else {
        VULCANOHTTP_LOG(err,
                        "There was an error while adding a new file descriptor to the EpollLoopper: {} - {}",
                        errno,
                        strerror(errno));
    }
}

void EpollLooper::removeFileDescriptor(int fd, EpollFileDescriptorRoles role) const
{
    epoll_event event{.events = EPOLLIN,
                      .data = {.u64 = static_cast<uint64_t>(fd) | static_cast<uint64_t>(role) << 32}};

    if (epoll_ctl(epollFileDescriptor, EPOLL_CTL_DEL, fd, &event) == -1) {

        VULCANOHTTP_LOG(info, "Connection successfully removed from EpollLooper!");
    } else {
        VULCANOHTTP_LOG(err,
                        "There was an error while removing a file descriptor from the EpollLooper:  {} - {}",
                        errno,
                        strerror(errno));
    }
}
EpollLooper::~EpollLooper()
{
    stop();
}
