/*
 * Copyright (C) 2021-2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/HTTPBinaryRepresentationUtils.h"
#include <arpa/inet.h>
#include "spdlog/spdlog.h"
#include <iostream>
#include <random>
#include <vulcanohttp/HTTPResponse.h>
#include <vulcanohttp/HTTPServer.h>

int main()
{
    HTTPServer ourHTTPServer("test-certificate.pem", "test-privatekey.pem", {{INADDR_ANY, 64456}, {in6addr_any, 64457}});
    ourHTTPServer.addRequestHandler("^/randomnumbergenerator$", [&ourHTTPServer](const HTTPRequest& request) {
        if (request.getHeaderFieldFromKey(":method").second == "GET") {
            // Generate a random number
            std::random_device rd;
            std::mt19937 mt(rd());
            std::uniform_int_distribution<int> dist(1, 10);

            std::string randomNumber = std::to_string(dist(mt));

            std::vector<unsigned char> bodyData(randomNumber.begin(), randomNumber.end());

            using HTTP::FrameUtils::HTTPHeaderField;
            using HTTP::FrameUtils::INDEXED_LITERAL;

            HTTPResponse(ourHTTPServer,
                                      request,
                                      {
                                          HTTPHeaderField{8}, // Set Response Status to 200
                                          {INDEXED_LITERAL, 54, "rng"},
                                          {INDEXED_LITERAL, 31, "text/plain; charset=utf-8"} // Set Content-Type
                                      },
                                      randomNumber)
                .send();
        } else {

            using HTTP::FrameUtils::HTTPHeaderField;
            using HTTP::FrameUtils::INDEXED_LITERAL;

            HTTPResponse(ourHTTPServer,
                                                     request,
                                                     {{INDEXED_LITERAL, 10, "405"}, // Show that method is not allowed
                                                      {INDEXED_LITERAL, 22, "GET"}, // Show allowed methods
                                                      {INDEXED_LITERAL, 54, "rng"}})
                .send();
        }
    });
    ourHTTPServer.start();
    std::cin.get(); // Waiting until user enters something, then stops server
    ourHTTPServer.stop();
    return 0;
}