# Contributing to VulcanoHTTP
Before contributing, please read the following repository rules at the bottom of this page.

To contribute please fork the branch and make your changes. Then you have to create a pull request. 
Please add a good title and description to it. If all works out right, your pull request will be merged.

If you want to report a bug create an issue and add the fitting labels. Please provide the version (or commit and build number) and operating system information and anything else that you think could be useful. Explain the bug in detail with clear structure.

For feature suggestions or feature improvements file an issue and add the fitting labels. Describe the feature in detail and try to clarify all possible questions.
